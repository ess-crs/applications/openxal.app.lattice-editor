/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import xal.tools.data.DataTable;
import xal.tools.data.EditContext;
import xal.tools.data.GenericRecord;
import xal.tools.data.InMemoryDataAdaptor;

/**
 * This class wraps around the {@link xal.tools.data.EditContext} tables to
 * provide JavaFX beans, and creates JavaFX TableView and TreeTableView objects
 * to display them.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class SimParamsTableWrapper {

    private static final List<Node> tables = new ArrayList<>();
    private final VBox parent;

    /**
     *
     * @param node
     */
    public SimParamsTableWrapper(VBox parent) {
        this.parent = parent;
    }

    public void loadEditContext(EditContext context) {
        // Clear previous tables from VBox.
        for (Node node : tables) {
            parent.getChildren().remove(node);
        }
        tables.clear();

        for (DataTable table : context.getTables()) {
            if (table.primaryKeys().size() == 1) {
                createTableView(table);
            } else {
                createTreeTableView(table);
            }
        }

        parent.getChildren().addAll(tables);
    }

    private void createTableView(DataTable table) {
        ObservableList<SimParamWrapper> elements = FXCollections.observableArrayList();

        // Add first row for default values
        SimParamWrapper defaultElement = new SimParamWrapper(table.attributes());
        elements.add(defaultElement);

        for (GenericRecord record : table.records()) {
            SimParamWrapper newElement = new SimParamWrapper(record, table.keys());
            elements.add(newElement);
        }

        // Create a TableView
        TableView<SimParamWrapper> tableView = new TableView<>(elements);
        tableView.setEditable(true);

        List<TableColumn> columns = new ArrayList<>();

        // Get the first and only key
        String firstKey = table.primaryKeys().iterator().next();
        // Create a new column for each key
        int i = 1;
        for (String columnName : table.keys()) {
            TableColumn<SimParamWrapper, String> newColumn = new TableColumn<>(columnName);
            newColumn.setCellValueFactory(p -> p.getValue().getProperty(columnName));
            newColumn.setCellFactory(TextFieldTableCell.forTableColumn());
            columns.add(newColumn);
            if (columnName.equals(firstKey)) {
                tableView.getColumns().add(0, newColumn);
            } else {
                tableView.getColumns().add(i, newColumn);
                i++;
            }
        }

        VBox vbox = new VBox();
        vbox.setPadding(Insets.EMPTY);

        // Creating + and - buttons for elements
        AddRemoveBox addRemoveElementBox = new AddRemoveBox("Add a new element", "Remove the element selected");
        addRemoveElementBox.setDisableRemoveButton(true);
        addRemoveElementBox.setAddButtonAction((event) -> {
            // Get parameter for primary key
            TextInputDialog dialog = new TextInputDialog("");
            dialog.setTitle("Adding a new parameter");
            dialog.setHeaderText("Adding a new parameter");
            dialog.setContentText("Please enter value for " + firstKey + ":");

            Optional<String> result = dialog.showAndWait();
            if (result.isPresent() && !result.get().isBlank()) {
                GenericRecord record = new GenericRecord(table);
                record.setValueForKey(result.get().trim(), firstKey);
                table.add(record);
                elements.add(new SimParamWrapper(record, table.keys()));
            }
        });

        tableView.getSelectionModel().selectedItemProperty().addListener((ov, t, t1) -> {
            if (tableView.getSelectionModel().getSelectedIndex() != 0) {
                addRemoveElementBox.setDisableRemoveButton(false);
            } else {
                addRemoveElementBox.setDisableRemoveButton(true);
            }
        });

        addRemoveElementBox.setRemoveButtonAction((event) -> {
            SimParamWrapper element = tableView.getSelectionModel().getSelectedItem();
            table.remove(element.getRecord());
            elements.remove(element);
        });

        vbox.getChildren().addAll(tableView, addRemoveElementBox);
        TitledPane titledPane = new TitledPane(table.name(), vbox);
        titledPane.expandedProperty().set(false);
        VBox.setMargin(tableView, Insets.EMPTY);
        tables.add(titledPane);
    }

    private void recursivelyModifyChildren(TreeItem<SimParamWrapper> parentItem, String key, String value) {
        for (TreeItem<SimParamWrapper> item : parentItem.getChildren()) {
            item.getValue().getProperty(key).setValue(value);
            recursivelyModifyChildren(item, key, value);
        }
    }

    private void buildTree(TreeItem<SimParamWrapper> parent, DataTable table, String[] primaryKeys, int index) {
        String key = primaryKeys[index];
        Collection<Object> values = table.getUniquePrimaryKeyValues(key);

        for (Object value : values) {
            if (index < primaryKeys.length - 1) {
                TreeItem<SimParamWrapper> newItem = addParentNode(key, value, parent);

                buildTree(newItem, table, primaryKeys, index + 1);
            } else {
                Map<String, String> keyValuePairs = new HashMap<>();
                keyValuePairs.put(key, value.toString());
                TreeItem<SimParamWrapper> newParent = parent;

                for (int i = index - 1; i >= 0; i--) {
                    String property = newParent.getValue().getProperty(primaryKeys[i]).getValue();
                    keyValuePairs.put(primaryKeys[i], property);
                    newParent = newParent.getParent();
                }
                GenericRecord record = table.genericRecord(keyValuePairs);
                if (record != null) {
                    SimParamWrapper newElem = new SimParamWrapper(record, table.keys());
                    TreeItem<SimParamWrapper> newItem = new TreeItem<>(newElem);
                    parent.getChildren().add(newItem);
                }
            }
        }
    }

    private TreeItem<SimParamWrapper> addParentNode(String key, Object value, TreeItem<SimParamWrapper> parent) {
        SimParamWrapper elem = new SimParamWrapper();
        elem.setProperty(key, value.toString());

        TreeItem<SimParamWrapper> newItem = new TreeItem<>(elem);
        parent.getChildren().add(newItem);
        newItem.setExpanded(true);

        // Bind property to records -> update the key for all elements
        StringProperty property = elem.getProperty(key);
        ChangeListener<String> cl = (ov, t, t1) -> {
            recursivelyModifyChildren(newItem, key, t1);
        };
        property.addListener(cl);

        return newItem;
    }

    private void createTreeTableView(DataTable table) {
        TreeItem<SimParamWrapper> elements = new TreeItem<>();

        // Get the primary keys
        Collection<String> primaryKeys = table.primaryKeys();
        String[] primaryKeysArray = primaryKeys.toArray(new String[0]);

        buildTree(elements, table, primaryKeysArray, 0);

        TreeTableView<SimParamWrapper> treeTable = new TreeTableView<>();
        treeTable.setRoot(elements);
        treeTable.setShowRoot(false);
        treeTable.setEditable(true);

        int primaryKeysIndex = 0;
        int otherKeysIndex = primaryKeys.size();
        // Create a new column for each key
        for (String columnName : table.keys()) {
            TreeTableColumn<SimParamWrapper, String> newColumn = new TreeTableColumn<>(columnName);
            newColumn.setCellValueFactory(p -> p.getValue().getValue().getProperty(columnName));
            newColumn.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());
            if (primaryKeys.contains(columnName)) {
                treeTable.getColumns().add(primaryKeysIndex, newColumn);
                primaryKeysIndex++;
            } else {
                treeTable.getColumns().add(otherKeysIndex, newColumn);
                otherKeysIndex++;
            }
        }

        VBox vbox = new VBox();
        vbox.setPadding(Insets.EMPTY);

        // Creating + and - buttons for elements
        AddRemoveBox addRemoveElementBox = new AddRemoveBox("Add a new element", "Remove the element selected");
        addRemoveElementBox.setDisableRemoveButton(true);
        addRemoveElementBox.setAddButtonAction((event) -> {
            // Create the custom dialog.
            Dialog<Map<String, String>> dialog = new Dialog<>();
            dialog.setTitle("Adding a new parameter");
            dialog.setHeaderText("Adding a new parameter");

            // Set the button types.
            dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

            // Create the username and password labels and fields.
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));

            int i = 0;
            List<TextField> textFields = new ArrayList<>();
            for (String key : primaryKeys) {
                TextField textField = new TextField();
                textFields.add(textField);
                grid.add(new Label(key), 0, i);
                grid.add(textField, 1, i);
                i++;
            }

            dialog.getDialogPane().setContent(grid);

            // Request focus on the first textfield by default.
            Platform.runLater(() -> textFields.get(0).requestFocus());

            // Convert the result to a username-password-pair when the login button is clicked.
            dialog.setResultConverter(dialogButton -> {
                if (dialogButton == ButtonType.OK) {
                    Map<String, String> result = new HashMap<>();
                    int j = 0;
                    for (String key : primaryKeys) {
                        result.put(key, textFields.get(j).getText());
                        j++;
                    }
                }
                return null;
            });

            Optional<Map<String, String>> result = dialog.showAndWait();

            if (result != null) {
                TreeItem<SimParamWrapper> parentItem = elements;
                TreeItem<SimParamWrapper> newParent;

                // Create a new GenericRecord
                GenericRecord record = new GenericRecord(table);
                InMemoryDataAdaptor dataAdaptor = new InMemoryDataAdaptor("temp");
                record.write(dataAdaptor);
                // Fill the record with the values of the primary keys, but stop if any field is empty.
                for (int k = 0; k < primaryKeys.size(); k++) {
                    if (textFields.get(k).getText().isBlank()) {
                        return;
                    }
                    dataAdaptor.setValue(primaryKeysArray[k], textFields.get(k).getText().trim());
                }
                record.update(dataAdaptor);

                // Create parent TreeItems if needed
                for (int k = 0; k < primaryKeys.size() - 1; k++) {
                    String key = primaryKeysArray[k];
                    String value = textFields.get(k).getText().trim();

                    newParent = null;
                    for (TreeItem<SimParamWrapper> child : parentItem.getChildren()) {
                        if (value.equals(child.getValue().getProperty(key).getValue())) {
                            newParent = child;
                            break;
                        }
                    }

                    if (newParent == null) {
                        newParent = addParentNode(key, value, parentItem);
                        parentItem = newParent;
                    } else {
                        parentItem = newParent;
                    }
                }
                // Finally add the new element to both the TreeTableView and the DataTable
                SimParamWrapper newElem = new SimParamWrapper(record, table.keys());
                TreeItem<SimParamWrapper> newItem = new TreeItem<>(newElem);
                parentItem.getChildren().add(newItem);
                table.add(record);
            }
        });

        treeTable.getSelectionModel().selectedItemProperty().addListener((ov, t, t1) -> {
            if (treeTable.getSelectionModel().getSelectedIndex() != 0) {
                addRemoveElementBox.setDisableRemoveButton(false);
            } else {
                addRemoveElementBox.setDisableRemoveButton(true);
            }
        });

        addRemoveElementBox.setRemoveButtonAction((event) -> {
            TreeItem<SimParamWrapper> element = treeTable.getSelectionModel().getSelectedItem();
            table.remove(element.getValue().getRecord());
            element.getParent().getChildren().remove(element);
        });

        vbox.getChildren().addAll(treeTable, addRemoveElementBox);

        TitledPane titledPane = new TitledPane(table.name(), vbox);

        titledPane.expandedProperty().set(false);
        VBox.setMargin(treeTable, Insets.EMPTY);

        tables.add(titledPane);
    }
}
