/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.io.File;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import xal.extension.fxapplication.FxApplication;
import xal.smf.Accelerator;
import xal.smf.data.XMLDataManager;

/**
 *
 * @author Juan F. Esteban Müller <juanf.estebanmuller@ess.eu>
 */
public class MainApp extends FxApplication {

    @Override
    public void setup(Stage stage) {
        // Set the main scene FXML file (in the resources directory)
        MAIN_SCENE = "/fxml/LatticeEditor.fxml";
        // Set the CSS style file (in the resources directory)
        CSS_STYLE = "/styles/Styles.css";
        // Set the application name for the title bar
        setApplicationName("Lattice Editor");
        // Set to false if this application doesn't save/load xml files
        HAS_DOCUMENTS = true;
        // Set to false if this application doesn't need the machine sequences
        HAS_SEQUENCE = true;
        // Setting the application document (remove if not used)
        DOCUMENT = new AppDocument(stage);
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void beforeStart(Stage stage) {
        // Remove Sequence menu from the Accelerator menu in the menubar.
        Menu acceleratorMenu = null;
        MenuItem sequenceItem = null;
        for (Menu menu : MENU_BAR.getMenus()) {
            if ("Accelerator".equals(menu.getText())) {
                acceleratorMenu = menu;
                break;
            }
        }
        if (acceleratorMenu != null) {
            for (MenuItem item : acceleratorMenu.getItems()) {
                if ("Sequence".equals(item.getText())) {
                    sequenceItem = item;
                    break;
                }
            }
        }
        if (sequenceItem != null) {
            acceleratorMenu.getItems().remove(sequenceItem);
        }

        MenuItem acceleratorFilesItem = new MenuItem("Modify accelerator and file names");
        acceleratorFilesItem.setOnAction((event) -> {
            AcceleratorFilesController dialog = AcceleratorFilesController.createAcceleratorFilesDialog();

            XMLDataManager acceleratorXMLManager = DOCUMENT.getAcceleratorXMLManager();

            dialog.setAcceleratorName(acceleratorXMLManager.getAccelerator().getSystemId());
            dialog.setDeviceMapping(acceleratorXMLManager.getDeviceMappingUrlSpec());
            dialog.setElementMapping(acceleratorXMLManager.getElementMappingUrlSpec());
            dialog.setOptics(acceleratorXMLManager.opticsUrlSpec());
            dialog.setTimingManager(acceleratorXMLManager.getTimingManagerUrlSpec());
            dialog.setHardwareStatus(acceleratorXMLManager.getHardwareStatusUrlSpec());
            dialog.setPowerSupplies(acceleratorXMLManager.getPowerSuppliesUrlSpec());
            dialog.setSimParams(acceleratorXMLManager.urlSpecForTableGroup("modelparams"));

            boolean result = dialog.show();
            if (result) {
                try {
                    Field m_strSysIdField = Accelerator.class.getDeclaredField("m_strSysId");
                    m_strSysIdField.setAccessible(true);
                    m_strSysIdField.set(DOCUMENT.getAccelerator(), dialog.getAcceleratorName());
                } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(SimParamWrapper.class.getName()).log(Level.SEVERE, null, ex);
                }

                acceleratorXMLManager.setDeviceMappingUrlSpec(dialog.getDeviceMapping());
                acceleratorXMLManager.setElementMappingUrlSpec(dialog.getElementMapping());
                acceleratorXMLManager.setOpticsUrlSpec(dialog.getOptics());
                acceleratorXMLManager.setTimingManagerUrlSpec(dialog.getTimingManager());
                acceleratorXMLManager.setHardwareStatusUrlSpec(dialog.getHardwareStatus());
                acceleratorXMLManager.setPowerSuppliesUrlSpec(dialog.getPowerSupplies());
                acceleratorXMLManager.setUrlSpecForTableGroup(dialog.getSimParams(), "modelparams");

                // Force reload of the accelerator tree view.
                DOCUMENT.setAccelerator(DOCUMENT.getAccelerator());
            }
        });

        acceleratorMenu.getItems().add(acceleratorFilesItem);
    }

    @Override
    protected void loadFileMenuHandler() {
        loadAcceleratorMenuHandler();
    }

    @Override
    protected void saveFileMenuHandler(boolean saveAs) {
        if (saveAs || !DOCUMENT.sourceSetAndValid()) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save Accelerator");
            fileChooser.setInitialFileName(DOCUMENT.getDefaultFilename());

            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XAL files (*.xal)", "*.xal");
            fileChooser.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File selectedFile = fileChooser.showSaveDialog(null);
            if (selectedFile != null) {
                DOCUMENT.setSource(selectedFile);
            }
        }
        if (DOCUMENT.sourceSetAndValid()) {
            DOCUMENT.saveDocument();
        }
    }
}
