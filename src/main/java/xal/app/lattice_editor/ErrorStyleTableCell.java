/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import javafx.css.PseudoClass;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.DefaultStringConverter;

/**
 * TableCell subclass
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 * @param <T>
 */
public class ErrorStyleTableCell<T extends ErrorValidation> extends TextFieldTableCell<T, String> {

    private PseudoClass ERROR = PseudoClass.getPseudoClass("error");

    ErrorStyleTableCell() {
        setConverter(new DefaultStringConverter());
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            pseudoClassStateChanged(ERROR, false);
        } else {
            T cell = (T) getTableRow().getItem();
            if (cell != null) {
                pseudoClassStateChanged(ERROR, cell.validate());
            }
        }
    }
}
