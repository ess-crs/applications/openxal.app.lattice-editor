/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class MultipleChoice {

    private StringProperty selected = new SimpleStringProperty();
    private List<String> choices;

    MultipleChoice(List<String> choices) {
        this.choices = choices;
    }

    public void select(String choice) {
        if (choices.contains(choice)) {
            selected.setValue(choice);
        } else {
            selected.setValue(null);
        }
    }

    public List<String> getChoices() {
        return choices;
    }

    public StringProperty selectedProperty() {
        return selected;
    }

    public String getSelected() {
        return selected.getValue();
    }
}
