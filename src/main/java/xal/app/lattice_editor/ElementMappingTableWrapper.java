/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import xal.extension.fxapplication.FxApplication;
import xal.model.IComponent;
import xal.sim.scenario.ElementMapping;
import xal.sim.scenario.FileBasedElementMapping;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNodeFactory;

/**
 * This class wraps around the {@link xal.sim.scenario.ElementMapping} object to
 * provide JavaFX beans, which can them be used by JavaFX applications.
 * <p>
 * The class also provides a method to represent the properties in a JavaFX
 * TableView and methods to add and remove element types.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class ElementMappingTableWrapper {

    private final ObservableList<ElementModelWrapper> defaultElementTypes = FXCollections.observableArrayList();
    private final ObservableList<ElementModelWrapper> elementTypes = FXCollections.observableArrayList();
    final FileBasedElementMapping elementMapping;
    private Map<String, Class<? extends IComponent>> classTable = null;
    private final AcceleratorNodeFactory nodeFactory;

    public ElementMappingTableWrapper(Accelerator accelerator) {
        elementMapping = (FileBasedElementMapping) accelerator.getElementMapping();
        nodeFactory = accelerator.getNodeFactory();

        Field ctField = null;
        try {
            ctField = ElementMapping.class.getDeclaredField("elementMap");
            ctField.setAccessible(true);
        } catch (NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            classTable = (Map<String, Class<? extends IComponent>>) ctField.get(elementMapping);
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(DeviceManagerTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
        }

        ElementModelWrapper defaultType = new ElementModelWrapper("Default element", elementMapping.getDefaultElementType(), classTable, accelerator.getNodeFactory());
        defaultType.classNameProperty().addListener((ChangeListener<String>) (ov, oldClassName, newClassName) -> {
            try {
                elementMapping.setDefaultElement(newClassName);
                defaultType.setError(false);
            } catch (ClassNotFoundException ex) {
                defaultType.setError(true);
            }
        });
        ElementModelWrapper defaultSequenceType = new ElementModelWrapper("Sequence", elementMapping.getDefaultSequenceType(), classTable, accelerator.getNodeFactory());
        defaultSequenceType.classNameProperty().addListener((ChangeListener<String>) (ov, oldClassName, newClassName) -> {
            try {
                elementMapping.setDefaultSequence(newClassName);
                defaultSequenceType.setError(false);
            } catch (ClassNotFoundException ex) {
                defaultSequenceType.setError(true);
            }
        });
        ElementModelWrapper defaultDriftType = new ElementModelWrapper("Drift", elementMapping.getDriftType(), classTable, accelerator.getNodeFactory());
        defaultDriftType.classNameProperty().addListener((ChangeListener<String>) (ov, oldClassName, newClassName) -> {
            try {
                elementMapping.setDrift(newClassName);
                defaultDriftType.setError(false);
            } catch (ClassNotFoundException ex) {
                defaultDriftType.setError(true);
            }
        });
        ElementModelWrapper defaultRfCavityDriftType = new ElementModelWrapper("RF Cavity Drift", elementMapping.getRfCavityDriftType(), classTable, accelerator.getNodeFactory());
        defaultRfCavityDriftType.classNameProperty().addListener((ChangeListener<String>) (ov, oldClassName, newClassName) -> {
            try {
                elementMapping.setRfCavityDrift(newClassName);
                defaultRfCavityDriftType.setError(false);
            } catch (ClassNotFoundException ex) {
                defaultRfCavityDriftType.setError(true);
            }
        });
        defaultElementTypes.add(defaultType);
        defaultElementTypes.add(defaultSequenceType);
        defaultElementTypes.add(defaultDriftType);
        defaultElementTypes.add(defaultRfCavityDriftType);

        for (String type : classTable.keySet()) {
            Class cls = classTable.get(type);

            ElementModelWrapper newType = new ElementModelWrapper(type, cls, classTable, accelerator.getNodeFactory());
            setBindings(newType);
            elementTypes.add(newType);
        }

    }

    private void setBindings(ElementModelWrapper elementModelWrapper) {
        /* Bindings */
        // Update type in ElementMapping.
        elementModelWrapper.typeProperty().addListener((ChangeListener<String>) (ov, oldType, newType) -> {
            try {
                if (this.classTable.containsKey(oldType)) {
                    this.classTable.remove(oldType);
                }
                this.classTable.put(newType, (Class) Class.forName(elementModelWrapper.getClassName()));
            } catch (ClassNotFoundException ex) {
            }
        });

        // Update ElementMapping when a valid class is entered.
        elementModelWrapper.classNameProperty().addListener(
                (ChangeListener<String>) (ov, oldClassName, newClassName) -> {
                    try {
                        Class newCls = Class.forName(newClassName);
                        if (this.classTable.containsKey(elementModelWrapper.getType())) {
                            this.classTable.remove(elementModelWrapper.getType());
                        }
                        this.classTable.put(elementModelWrapper.getType(), newCls);
                        elementModelWrapper.setError(false);
                    } catch (ClassNotFoundException ex) {
                        elementModelWrapper.setError(true);
                    }
                }
        );
    }

    public List<String> getTypesForClass(Class cls) {
        List<String> types = new ArrayList<>();

        for (ElementModelWrapper dt : elementTypes) {
            if (dt.getClassName().equals(cls.getCanonicalName())) {
                types.add(dt.getType());
            }
        }

        return types;
    }

    public Class getClassForTypes(String type) {
        return classTable.get(type);
    }

    public void setDefaultElementsTableView(TableView table, TableColumn iconColumn, TableColumn typeColumn, TableColumn classNameColumn) {
        table.itemsProperty().setValue(defaultElementTypes);

        table.setEditable(true);

        iconColumn.setCellValueFactory(new PropertyValueFactory<>("icon"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        classNameColumn.setCellValueFactory(new PropertyValueFactory<>("className"));

        iconColumn.setCellFactory(c -> new ImageTableCell<>());
        typeColumn.setCellFactory(NonEditableCell.forTableColumn());
        classNameColumn.setCellFactory(c -> new ErrorStyleTableCell<ElementModelWrapper>());

    }

    public void setTableView(TableView table, TableColumn iconColumn, TableColumn typeColumn, TableColumn classNameColumn) {
        table.itemsProperty().setValue(elementTypes);

        table.setEditable(true);

        iconColumn.setCellValueFactory(new PropertyValueFactory<>("icon"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        classNameColumn.setCellValueFactory(new PropertyValueFactory<>("className"));

        iconColumn.setCellFactory(c -> new ImageTableCell<>());
        typeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        classNameColumn.setCellFactory(c -> new ErrorStyleTableCell<ElementModelWrapper>());
    }

    public void addElementType() {
        AddDeviceTypeDialog dialog = new AddDeviceTypeDialog();
        Optional<ElementModelWrapper> result = dialog.showAndWait();

        result.ifPresent(newType -> {
            try {
                this.classTable.put(newType.typeProperty().getValue(), (Class) Class.forName(newType.classNameProperty().getValue()));
                elementTypes.add(newType);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DeviceManagerTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void removeElementType(String type) {
        for (ElementModelWrapper dtw : elementTypes) {
            if (dtw.typeProperty().getValue().equals(type)) {
                elementTypes.remove(dtw);
                break;
            }
        }
        if (this.classTable.containsKey(type)) {
            this.classTable.remove(type);
        }
    }

    class AddDeviceTypeDialog extends Dialog<ElementModelWrapper> {

        private final PseudoClass ERROR = PseudoClass.getPseudoClass("error");

        private TextField typeField;
        private TextField classField;
        Label typeError;
        Label classError;

        AddDeviceTypeDialog() {
            FxApplication.setTheme(getDialogPane().getScene());

            setTitle("Add a new Element Type");
            setHeaderText("Add a new Element Type");

            // Set the button types.
            getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

            // Create the text fields.
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 10, 10, 10));

            typeField = new TextField();
            typeField.setPromptText("Type");
            classField = new TextField();
            classField.setPromptText("Class name");

            typeError = new Label();
            typeError.setPrefWidth(150);
            typeError.setText("");
            classError = new Label();
            classError.setPrefWidth(150);
            classError.setText("");

            grid.add(new Label("Type:"), 0, 0);
            grid.add(typeField, 1, 0);
            grid.add(typeError, 2, 0);
            grid.add(new Label("Class name:"), 0, 1);
            grid.add(classField, 1, 1);
            grid.add(classError, 2, 1);

            // Enable/Disable OK button depending on whether a type and class name were entered.
            Node okButton = getDialogPane().lookupButton(ButtonType.OK);
            okButton.setDisable(true);

            // Do some validation.
            typeField.textProperty().addListener((observable, oldValue, newValue) -> {
                boolean error = validateType(newValue.trim());
                typeField.pseudoClassStateChanged(ERROR, error);

                error |= validateClass(classField.getText().trim());

                okButton.setDisable(error);
            });

            classField.textProperty().addListener((observable, oldValue, newValue) -> {
                boolean error = validateClass(newValue.trim());
                classField.pseudoClassStateChanged(ERROR, error);

                error |= validateType(typeField.getText().trim());

                okButton.setDisable(error);
            });

            getDialogPane().setContent(grid);

            // Request focus on the type field by default.
            Platform.runLater(() -> typeField.requestFocus());

            // Convert the result to a ElementModelWrapper when the OK button is clicked.
            setResultConverter(dialogButton -> {
                if (dialogButton == ButtonType.OK) {
                    try {
                        return new ElementModelWrapper(typeField.getText().trim(), Class.forName(classField.getText().trim()), classTable, nodeFactory);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(ElementMappingTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return null;
            });
        }

        private boolean validateType(String type) {
            boolean error = true;
            String message = "";

            if (!type.equals("")) {
                if (classTable.containsKey(type)) {
                    message = "Type already exists.";
                } else {
                    error = false;
                }
            }

            typeError.setText(message);
            return error;
        }

        private boolean validateClass(String className) {
            boolean error = false;
            String message = "";

            try {
                Class cls = Class.forName(className);
            } catch (ClassNotFoundException ex) {
                error = true;
                message = "Class not found.";
            }

            classError.setText(message);
            return error;
        }
    }
}
