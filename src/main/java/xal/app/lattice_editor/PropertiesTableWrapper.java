/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;
import xal.smf.impl.Electromagnet;
import xal.tools.data.InMemoryDataAdaptor;

/**
 * This class wraps around the main properties of the
 * {@link xal.smf.AcceleratorNode} object to provide JavaFX beans, which can
 * them be used by JavaFX applications.
 * <p>
 * The class also provides a method to represent the properties in a JavaFX
 * TableView.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class PropertiesTableWrapper {

    private final ObservableList<PropertyWrapper> properties = FXCollections.observableArrayList();
    private final List<PropertyListener> listeners = new ArrayList<>();
    private final List<PropertyListener> psListeners = new ArrayList<>();
    private AcceleratorNode node;

    /**
     *
     * @param node
     * @param deviceManagerTableWrapper
     * @param powerSupplies
     */
    public PropertiesTableWrapper(AcceleratorNode node, DeviceManagerTableWrapper deviceManagerTableWrapper, ObservableList<String> powerSupplies) {
        this.node = node;

        PropertyWrapper nodeIdProperty = new PropertyWrapper("Node ID", node.getId());
        PropertyWrapper positionProperty = new PropertyWrapper("Position", Double.toString(node.getPosition()));
        PropertyWrapper lengthProperty = new PropertyWrapper("Length", Double.toString(node.getLength()));
        PropertyWrapper sDisplayProperty = new PropertyWrapper("SDisplay", Double.toString(node.getSDisplay()));
        MultipleChoice types = new MultipleChoice(deviceManagerTableWrapper.getTypesForClass(node.getClass()));
        types.select(node.getType());
        PropertyWrapper typeProperty = new PropertyWrapper("Type", types);
        MultipleChoice softTypes = new MultipleChoice(deviceManagerTableWrapper.getSoftTypesForType(node.getType()));
        if (node.getSoftType() != null && !node.getSoftType().equals("")) {
            softTypes.select(node.getSoftType());
        }
        PropertyWrapper softTypeProperty = new PropertyWrapper("SoftType", softTypes);
        PropertyWrapper eIdProperty = new PropertyWrapper("Engineering ID", node.getEId());
        PropertyWrapper pIdProperty = new PropertyWrapper("Physics ID", node.getPId());
        PropertyWrapper statusProperty = new PropertyWrapper("Status", (Boolean) node.getStatus());
        PropertyWrapper validProperty = new PropertyWrapper("Valid", (Boolean) node.getValid());

        properties.addAll(nodeIdProperty, positionProperty, lengthProperty, sDisplayProperty, typeProperty, softTypeProperty, eIdProperty, pIdProperty, statusProperty, validProperty);

        // Bind properties to AcceleratorNode fields.
        nodeIdProperty.valueProperty().addListener((ChangeListener<String>) (ov, t, t1) -> {
            (new NodeIds(node)).setId(t1);

            for (PropertyListener listener : listeners) {
                listener.changed(node, node);
            }
        });
        positionProperty.valueProperty().addListener((ChangeListener<String>) (ov, t, t1) -> node.setPosition(Double.parseDouble(t1)));
        lengthProperty.valueProperty().addListener((ChangeListener<String>) (ov, t, t1) -> node.setLength(Double.parseDouble(t1)));
        eIdProperty.valueProperty().addListener((ChangeListener<String>) (ov, t, t1) -> (new NodeIds(node)).setEId(t1));
        pIdProperty.valueProperty().addListener((ChangeListener<String>) (ov, t, t1) -> (new NodeIds(node)).setPId(t1));
        statusProperty.valueProperty().addListener((ChangeListener<Boolean>) (ov, t, t1) -> node.setStatus(t1));
        validProperty.valueProperty().addListener((ChangeListener<Boolean>) (ov, t, t1) -> node.setValid(t1));

        types.selectedProperty().addListener((ChangeListener<String>) (ov, t, t1) -> {
            if (!t.equals(t1))
                try {
                AcceleratorNode oldNode = this.node;

                // Create a new element of the new type
                Class newClass = deviceManagerTableWrapper.getClassForTypes(t1);
                Constructor constructor = newClass.getConstructor(new Class[]{String.class});
                AcceleratorNode newNode = (AcceleratorNode) constructor.newInstance(this.node.getId());

                // Update soft types
                MultipleChoice newSoftTypes = new MultipleChoice(deviceManagerTableWrapper.getSoftTypesForType(t1));
                String softType = newNode.getSoftType() != null ? newNode.getSoftType() : "";
                newSoftTypes.select(softType);

                // Transfer parameters from old node to the new one
                InMemoryDataAdaptor da = new InMemoryDataAdaptor(node.getId());
                this.node.write(da);
                da.setValue("type", t1);
                da.setValue("softType", softType);
                newNode.update(da);

                // Remove the old node and add the new one
                AcceleratorSeq parent = this.node.getParent();
                int nodeIndex = parent.getIndexOfNode(this.node);
                parent.removeNode(this.node);
                parent.addNodeAt(nodeIndex, newNode);

                this.node = newNode;
                for (PropertyListener listener : listeners) {
                    listener.changed(oldNode, newNode);
                }
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(PropertiesTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        softTypes.selectedProperty().addListener((ChangeListener<String>) (ov, t, t1) -> {
            if (t == null || t1 == null || !t.equals(t1))
                try {
                AcceleratorNode oldNode = this.node;
                // Create a new element of the new type
                String type = ((MultipleChoice) typeProperty.valueProperty().getValue()).getSelected();
                String fullType = type;
                String softType = t1;
                if (softType != null && !softType.equals("")) {
                    fullType = type.concat(".").concat(softType);
                }
                Class newClass = deviceManagerTableWrapper.getClassForTypes(fullType);
                Constructor constructor = newClass.getConstructor(new Class[]{String.class});
                AcceleratorNode newNode = (AcceleratorNode) constructor.newInstance(this.node.getId());

                // Transfer parameters from old node to the new one
                InMemoryDataAdaptor da = new InMemoryDataAdaptor(node.getId());
                this.node.write(da);
                da.setValue("type", type);
                da.setValue("softType", t1 == null ? "" : t1);
                newNode.update(da);

                // Remove the old node and add the new one
                AcceleratorSeq parent = this.node.getParent();
                int nodeIndex = parent.getIndexOfNode(this.node);
                parent.removeNode(this.node);
                parent.addNodeAt(nodeIndex, newNode);

                this.node = newNode;
                for (PropertyListener listener : listeners) {
                    listener.changed(oldNode, newNode);
                }
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(PropertiesTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        if (node instanceof Electromagnet) {
            PropertyWrapper powerSupplyProperty;

            MultipleChoice powerSuppliesMC = new MultipleChoice(powerSupplies);
            if (((Electromagnet) node).getMainSupply() != null) {
                powerSuppliesMC.select(((Electromagnet) node).getMainSupply().getId());
            }
            powerSupplyProperty = new PropertyWrapper("Power Supply", powerSuppliesMC);

            // Update the list of power supplies when it changes.
            powerSupplies.addListener((ListChangeListener) (c) -> {
                MultipleChoice newPowerSuppliesMC = new MultipleChoice(powerSupplies);
                if (((Electromagnet) node).getMainSupply() != null) {
                    newPowerSuppliesMC.select(((Electromagnet) node).getMainSupply().getId());
                }
                powerSupplyProperty.valueProperty().setValue(newPowerSuppliesMC);
            });

            properties.add(powerSupplyProperty);
            
            powerSuppliesMC.selectedProperty().addListener((ChangeListener<String>) (ov, t, t1) -> {
                ((Electromagnet) node).setMainSupplyId(t1);
                psListeners.forEach(listener -> listener.changed(node, node));
            });
        }
    }

    public void setTableView(TableView table, TableColumn nameColumn, TableColumn valueColumn) {
        table.setEditable(true);
        table.getSelectionModel().cellSelectionEnabledProperty().set(true);

        nameColumn.setCellFactory(NonEditableCell.forTableColumn());

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        valueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

        Callback<TableColumn<PropertyWrapper, Object>, TableCell<PropertyWrapper, Object>> clbck = new Callback<>() {
            @Override
            public TableCell<PropertyWrapper, Object> call(TableColumn<PropertyWrapper, Object> column) {
                TableCell<PropertyWrapper, Object> cell = new FlexibleTableCell();
                cell.itemProperty().addListener((obs, oldValue, newValue) -> {
                    cell.setEditable(false);
                    TableRow row = cell.getTableRow();
                    if (row != null) {
                        PropertyWrapper property = (PropertyWrapper) row.getItem();
                        if (property != null && !"SDisplay".equals(property.nameProperty().getValue())) {
                            cell.setEditable(true);
                        } else {
                            cell.setStyle("-fx-font-weight: bold;");
                        }
                    }
                });
                return cell;
            }
        };
        valueColumn.setCellFactory(clbck);
        table.itemsProperty().setValue(properties);
    }

    public void addNodeChangedListener(PropertyListener il) {
        listeners.add(il);
    }

    public void removeNodeChangedListener(PropertyListener il) {
        listeners.remove(il);
    }

    public void addPSChangedListener(PropertyListener il) {
        psListeners.add(il);
    }

    public void removePSChangedListener(PropertyListener il) {
        psListeners.remove(il);
    }

    public interface PropertyListener {

        public void changed(AcceleratorNode nodeBefore, AcceleratorNode nodeAfter);
    }

    /**
     * Utility class to access AcceleratorNode private fields using reflections.
     */
    class NodeIds {

        private final AcceleratorNode node;

        NodeIds(AcceleratorNode node) {
            this.node = node;
        }

        void setId(String id) {
            try {
                Field m_strIdField = AcceleratorNode.class.getDeclaredField("m_strId");
                m_strIdField.setAccessible(true);
                m_strIdField.set(node, id);
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(PropertiesTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        void setPId(String pId) {
            try {
                Field _strIdField = AcceleratorNode.class.getDeclaredField("m_strPId");
                _strIdField.setAccessible(true);
                _strIdField.set(node, pId);
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(PropertiesTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        void setEId(String eId) {
            try {
                Field _strIdField = AcceleratorNode.class.getDeclaredField("m_strEId");
                _strIdField.setAccessible(true);
                _strIdField.set(node, eId);
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(PropertiesTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
