/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import xal.extension.fxapplication.FxApplication;
import xal.extension.fxapplication.widgets.AcceleratorTreeView;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;

/**
 * FXML Controller class
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class NewElementController implements Initializable {

    @FXML
    private TextField elementNameTF;
    @FXML
    private VBox treePane;
    @FXML
    private TableView<DeviceTypeWrapper> deviceManagerTV;
    @FXML
    private TableColumn<?, ?> iconColumnDM;
    @FXML
    private TableColumn<?, ?> typeColumDM;
    @FXML
    private TableColumn<?, ?> softTypeColumDM;
    @FXML
    private TableColumn<?, ?> classColumnDM;
    @FXML
    private Button okButton;
    @FXML
    private Button cancelButton;

    private AcceleratorTreeView modelTreeView;

    private Parent root;

    private String nodeId;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        elementNameTF.textProperty().addListener((ov, t, t1) -> {
            if (!t1.isBlank() && modelTreeView.getSelectedItem() != null && deviceManagerTV.getSelectionModel().getSelectedItem() != null) {
                okButton.setDisable(false);
            } else {
                okButton.setDisable(true);
            }
        });

        okButton.setOnAction((ev) -> addNewElement());
        cancelButton.setOnAction((ev) -> close());
    }

    public static NewElementController createNewElementDialog(Accelerator accelerator, DeviceManagerTableWrapper deviceManagerTableWrapper) {
        NewElementController controller = null;

        FXMLLoader fxmlLoader = new FXMLLoader(FXMLController.class.getResource("/fxml/NewElement.fxml"));
        try {
            Parent root = (Parent) fxmlLoader.load();

            controller = fxmlLoader.<NewElementController>getController();
            controller.setRoot(root);
            controller.setAccelerator(accelerator);
            controller.setDeviceManager(deviceManagerTableWrapper);

        } catch (IOException ex) {
            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return controller;
    }

    public void setRoot(Parent root) {
        this.root = root;
    }

    public String show() {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Add new element");
        stage.setScene(new Scene(root));
        FxApplication.setTheme(root.getScene());
        stage.showAndWait();

        return nodeId;
    }

    private void addNewElement() {
        DeviceTypeWrapper deviceType = deviceManagerTV.getSelectionModel().getSelectedItem();
        try {
            Class nodeClass = Class.forName(deviceType.getClassName());
            Constructor constructor = nodeClass.getConstructor(new Class[]{String.class});
            nodeId = elementNameTF.getText();
            AcceleratorNode node = (AcceleratorNode) constructor.newInstance(nodeId);
            // Add basic channels - with empty signals
            Collection<String> defaultHandles = node.getDefaultHandles();
            for (String handle : defaultHandles) {
                node.channelSuite().putChannel(handle, "", true);
            }
            AcceleratorSeq sequence = (AcceleratorSeq) modelTreeView.getSelectedNode();
            sequence.addNode(node);
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(NewElementController.class.getName()).log(Level.SEVERE, null, ex);
        }

        close();
    }

    private void close() {
        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();
    }

    protected void setDeviceManager(DeviceManagerTableWrapper deviceManagerTableWrapper) {
        deviceManagerTableWrapper.setTableView(deviceManagerTV, iconColumnDM, typeColumDM, softTypeColumDM, classColumnDM);

        deviceManagerTV.getSelectionModel().selectedItemProperty().addListener((ov, t, t1) -> {
            if (!elementNameTF.getText().isBlank() && modelTreeView.getSelectedItem() != null) {
                okButton.setDisable(false);
            }
        });
    }

    protected void setAccelerator(Accelerator accelerator) {
        modelTreeView = new AcceleratorTreeView();
        modelTreeView.update(accelerator);
        modelTreeView.hideFilterMenu();
        modelTreeView.deselectAllFilters();
        modelTreeView.setAlwaysShowRfCavities(true);
        modelTreeView.getTitlebar().getChildren().clear();
        modelTreeView.setShowAcceleratorNode(true);

        treePane.getChildren().add(modelTreeView);

        modelTreeView.selectedItemProperty().addListener((ChangeListener<TreeItem<AcceleratorNode>>) (ov, t, t1) -> {
            if (!elementNameTF.getText().isBlank() && deviceManagerTV.getSelectionModel().getSelectedItem() != null) {
                okButton.setDisable(false);
            }
        });
    }
}
