/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.net.URL;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import xal.extension.fxapplication.widgets.AcceleratorNodeIcon;
import xal.smf.AcceleratorNodeFactory;
import xal.smf.AcceleratorSeq;
import xal.smf.impl.RfCavity;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class DeviceTypeWrapper implements ErrorValidation {

    private ObjectProperty icon = new SimpleObjectProperty();
    StringProperty type = new SimpleStringProperty("");
    StringProperty softType = new SimpleStringProperty("");
    StringProperty className = new SimpleStringProperty("");
   
    private boolean error = false;

    DeviceTypeWrapper(String type, String softType, Class cls, AcceleratorNodeFactory nodeFactory) {
        URL iconPath = AcceleratorNodeIcon.class.getResource("icons/32/" + AcceleratorNodeIcon.getIcon(cls));
        // Icon by default when missing.
        if (iconPath == null) {
            if (RfCavity.class.isAssignableFrom(cls)) {
                iconPath = AcceleratorNodeIcon.class.getResource("icons/32/CAVM.png");
            } else if (AcceleratorSeq.class.isAssignableFrom(cls)) {
                iconPath = AcceleratorNodeIcon.class.getResource("icons/32/SEQ.png");
            } else {
                iconPath = AcceleratorNodeIcon.class.getResource("icons/32/BBX.png");
            }
        }

        Image iconImage = new Image(iconPath.toExternalForm());

        icon.setValue(iconImage);
        this.type.setValue(type);
        this.softType.setValue(softType);
        this.className.setValue(cls.getCanonicalName());
    }

    public ObjectProperty iconProperty() {
        return icon;
    }

    public String getType() {
        return type.getValue();
    }

    public String getSoftType() {
        return softType.getValue();
    }

    public String getClassName() {
        return className.getValue();
    }

    public StringProperty typeProperty() {
        return type;
    }

    public StringProperty softTypeProperty() {
        return softType;
    }

    public StringProperty classNameProperty() {
        return className;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    @Override
    public boolean validate() {
        return error;
    }
}
