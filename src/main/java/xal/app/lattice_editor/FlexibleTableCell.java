/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;

/**
 * TableCell subclass that implements normal text field, a checkbox, or a
 * combobox depending of the Property type.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class FlexibleTableCell extends TableCell<PropertyWrapper, Object> {

    private TextField textField;
    private CheckBox checkBox;
    private ComboBox<String> comboBox;

    public FlexibleTableCell() {
        textField = new TextField();
        checkBox = new CheckBox();
        comboBox = new ComboBox<>();

        checkBox.selectedProperty().addListener((ChangeListener<Boolean>) (observable, oldValue, newValue) -> {
            checkBox.setAlignment(Pos.CENTER);
        });

        comboBox.getSelectionModel().selectedItemProperty().addListener((ChangeListener<String>) (observable, oldValue, newValue) -> {
            if (newValue != null) {
                ((MultipleChoice) getItem()).select(newValue);
            }
        });
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        if (getItem() instanceof String) {
            setText((String) getItem());
        }
        setGraphic(null);
    }

    @Override
    public void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {
            if (item instanceof Boolean) {
                setupCheckBox();
                setText(null);
                setGraphic(checkBox);
                checkBox.setSelected(getBoolean());
            } else if (item instanceof MultipleChoice) {
                MultipleChoice mcItem = (MultipleChoice) item;
                // If there is only one choice, don't show a combobox, but only set the text.
                if (mcItem.getChoices().size() <= 1) {
                    setText(mcItem.getSelected());
                    setGraphic(null);
                } else {
                    setupComboBox((MultipleChoice) item);
                    setText(null);
                    setGraphic(comboBox);
                    comboBox.prefWidthProperty().bind(widthProperty());
                }
            } else {
                if (isEditing()) {
                    textField.setText(getString());
                    setText(null);
                    setGraphic(textField);
                } else {
                    setText(getString());
                    setGraphic(null);
                }
            }

            if (getGraphic() != null) {
                setAlignment(Pos.CENTER);
            }
        }
    }

    private void setupCheckBox() {
        getStyleClass().add("check-box-table-cell");
        checkBox.setAlignment(Pos.CENTER);

        checkBox.setSelected(getBoolean());
        if (isEditable()) {
            checkBox.setDisable(false);
        } else {
            checkBox.setDisable(true);
        }
    }

    private void setupComboBox(MultipleChoice item) {
        getStyleClass().add("combo-box-table-cell");

        if (!item.getChoices().isEmpty()) {
            ObservableList<String> observableArrayList
                    = FXCollections.observableArrayList(item.getChoices());
            comboBox.setItems(observableArrayList);
            comboBox.getSelectionModel().select(item.getSelected());
        }
    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

    private Boolean getBoolean() {
        return getItem() == null ? false : (Boolean) getItem();
    }
}
