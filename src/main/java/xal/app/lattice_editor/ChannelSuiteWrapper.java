/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import xal.ca.Channel;
import xal.ca.ChannelTimeRecord;
import xal.ca.ConnectionException;
import xal.ca.ConnectionListener;
import xal.ca.IEventSinkValTime;
import xal.ca.Monitor;
import xal.ca.MonitorException;
import xal.ca.PutException;
import xal.smf.AcceleratorNode;
import xal.smf.ChannelSuite;
import xal.smf.impl.Electromagnet;
import xal.tools.ArrayValue;

/**
 * This class wraps around the Channel Suite of the
 * {@link xal.smf.AcceleratorNode} object to provide JavaFX beans, which can
 * them be used by JavaFX applications.
 * <p>
 * The class also provides a method to represent the properties in a JavaFX
 * TableView.
 * <p>
 * Note: the monitors are cleared when a new ChannelSuiteWrapper instance is
 * created.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class ChannelSuiteWrapper {

    protected final ObservableList<ChannelWrapper> channels = FXCollections.observableArrayList();
    protected static final Map<String, SignalMonitor> monitors = new HashMap<>();
    protected final List<ChannelSuite> channelSuites = new ArrayList<>();

    public ChannelSuiteWrapper() {
    }

    /**
     *
     * @param node
     */
    public ChannelSuiteWrapper(AcceleratorNode node) {
        clearMonitors();

        addChannelSuite(node.channelSuite());

        if (node instanceof Electromagnet) {
            if (((Electromagnet) node).getMainSupply() != null) {
                addChannelSuite(((Electromagnet) node).getMainSupply().getChannelSuite());
            }
        }
    }

    protected final void clearMonitors() {
        for (SignalMonitor monitor : monitors.values()) {
            monitor.clear();
        }
        monitors.clear();
    }

    protected final void addChannelSuite(ChannelSuite channelSuite) {
        channelSuites.add(channelSuite);
        for (String handle : channelSuite.getHandles()) {
            ChannelWrapper channelWrapper = new ChannelWrapper(handle, channelSuite.getSignal(handle), channelSuite.isValid(handle), channelSuite.isSettable(handle));
            channels.add(channelWrapper);
            // Bind properties to SignalEntry fields.
            channelWrapper.signalProperty().addListener((ChangeListener<String>) (ObservableValue<? extends String> ov, String t, String t1) -> {
                // If signal changes, restart the monitor.
                monitors.get(handle).clear();
                new SignalMap(channelSuite).setSignal(handle, t1);
                monitors.get(handle).start();
            });
            channelWrapper.validProperty().addListener((ChangeListener<Boolean>) (ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> new SignalMap(channelSuite).setValid(handle, t1));
            channelWrapper.settableProperty().addListener((ChangeListener<Boolean>) (ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> channelSuite.setSettable(handle, t1));
            channelWrapper.liveValueProperty().setValue("NOT CONNECTED");

            // Create channel monitor to display the live value.
            SignalMonitor signalMonitor = new SignalMonitor(channelWrapper);
            signalMonitor.start();
            monitors.put(handle, signalMonitor);
        }
    }

    public void setTableView(TableView table, TableColumn handleColumn, TableColumn signalColumn, TableColumn validColumn, TableColumn settableColumn, TableColumn liveValueColumn) {
        table.itemsProperty().setValue(channels);
        table.setEditable(true);
        table.getSelectionModel().cellSelectionEnabledProperty().set(true);

        handleColumn.setCellValueFactory(new PropertyValueFactory<>("handle"));
        signalColumn.setCellValueFactory(new PropertyValueFactory<>("signal"));
        validColumn.setCellValueFactory(new PropertyValueFactory<>("valid"));
        settableColumn.setCellValueFactory(new PropertyValueFactory<>("settable"));
        liveValueColumn.setCellValueFactory(new PropertyValueFactory<>("liveValue"));

        handleColumn.setCellFactory(NonEditableCell.forTableColumn());
        signalColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        validColumn.setCellFactory(CheckBoxTableCell.forTableColumn(validColumn));
        settableColumn.setCellFactory(CheckBoxTableCell.forTableColumn(settableColumn));

        Callback<TableColumn<ChannelWrapper, String>, TableCell<ChannelWrapper, String>> clbck = new Callback<>() {
            @Override
            public TableCell<ChannelWrapper, String> call(TableColumn<ChannelWrapper, String> column) {
                TableCell<ChannelWrapper, String> cell = TextFieldTableCell.<ChannelWrapper>forTableColumn().call(column);
                cell.itemProperty().addListener((obs, oldValue, newValue) -> {
                    TableRow row = cell.getTableRow();
                    if (row != null) {
                        ChannelWrapper channel = (ChannelWrapper) row.getItem();
                        if (channel != null) {
                            // Set disabled style if channel not connected.
                            ChangeListener<? super Boolean> listener = (ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
                                if (t1) {
                                    cell.setEditable(true);
                                    cell.setStyle("");
                                } else {
                                    cell.setEditable(false);
                                    cell.setStyle(" -fx-font-weight: bold;");
                                }
                            };
                            // Run listener once to apply to current value
                            listener.changed(null, false, channel.connectedProperty().getValue());
                            channel.connectedProperty().addListener(listener);
                        }
                    }
                });
                return cell;
            }
        };
        liveValueColumn.setCellFactory(clbck);
    }

    interface ObjectGetter {

        public Object get(ChannelTimeRecord ctr);
    }

    interface ObjectSetter {

        public void put(String string) throws ConnectionException, PutException;
    }

    /**
     * This class creates a monitor that connects to the PVs. It updates the
     * values showed in the table and allows to put values to the PVs.
     */
    class SignalMonitor implements IEventSinkValTime, ConnectionListener {

        ChannelWrapper channelWrapper;
        Channel channel;
        ChangeListener<String> propertyListener;
        Class<?> dataType;
        final Object lock = new Object();
        Object lastValue;
        private final Map<Class, ObjectGetter> getObjectMap = new HashMap<>();
        private final Map<Class, ObjectSetter> putObjectMap = new HashMap<>();
        private Monitor monitor;

        SignalMonitor(ChannelWrapper channelWrapper) {
            this.channelWrapper = channelWrapper;

            // Map to get any type of data and store it as Object.
            getObjectMap.put(String.class, (ChannelTimeRecord ctr) -> ctr.stringValue());
            getObjectMap.put(byte.class, (ChannelTimeRecord ctr) -> ctr.byteValue());
            getObjectMap.put(short.class, (ChannelTimeRecord ctr) -> ctr.shortValue());
            getObjectMap.put(int.class, (ChannelTimeRecord ctr) -> ctr.intValue());
            getObjectMap.put(long.class, (ChannelTimeRecord ctr) -> ctr.longValue());
            getObjectMap.put(float.class, (ChannelTimeRecord ctr) -> ctr.floatValue());
            getObjectMap.put(double.class, (ChannelTimeRecord ctr) -> ctr.doubleValue());
            getObjectMap.put(String[].class, (ChannelTimeRecord ctr) -> ctr.stringArray());
            getObjectMap.put(byte[].class, (ChannelTimeRecord ctr) -> ctr.byteArray());
            getObjectMap.put(short[].class, (ChannelTimeRecord ctr) -> ctr.shortArray());
            getObjectMap.put(int[].class, (ChannelTimeRecord ctr) -> ctr.intArray());
            getObjectMap.put(long[].class, (ChannelTimeRecord ctr) -> ctr.longArray());
            getObjectMap.put(float[].class, (ChannelTimeRecord ctr) -> ctr.floatArray());
            getObjectMap.put(double[].class, (ChannelTimeRecord ctr) -> ctr.doubleArray());
            // Map to put any type of data from a String.
            putObjectMap.put(String.class, (String string) -> channel.putVal(string));
            putObjectMap.put(byte.class, (String string) -> channel.putVal(Byte.parseByte(string)));
            putObjectMap.put(short.class, (String string) -> channel.putVal(Short.parseShort(string)));
            putObjectMap.put(int.class, (String string) -> channel.putVal(Integer.parseInt(string)));
            putObjectMap.put(long.class, (String string) -> channel.putVal(Long.parseLong(string)));
            putObjectMap.put(float.class, (String string) -> channel.putVal(Float.parseFloat(string)));
            putObjectMap.put(double.class, (String string) -> channel.putVal(Double.parseDouble(string)));
            putObjectMap.put(String[].class, (String string) -> channel.putVal(toStringArray(string)));
            putObjectMap.put(byte[].class, (String string) -> channel.putVal(toStringStore(string).byteArray()));
            putObjectMap.put(short[].class, (String string) -> channel.putVal(toStringStore(string).shortArray()));
            putObjectMap.put(int[].class, (String string) -> channel.putVal(toStringStore(string).intArray()));
            putObjectMap.put(long[].class, (String string) -> channel.putVal(toStringStore(string).longArray()));
            putObjectMap.put(float[].class, (String string) -> channel.putVal(toStringStore(string).floatArray()));
            putObjectMap.put(double[].class, (String string) -> channel.putVal(toStringStore(string).doubleArray()));
        }

        public void start() {
            for (ChannelSuite channelSuite : channelSuites) {
                channel = channelSuite.getChannel(channelWrapper.handleProperty().getValue());

                if (channel != null) {
                    channel.addConnectionListener(this);
                    channel.requestConnection();
                    break;
                }
            }
        }

        private String[] toStringArray(String string) {
            return string.substring(1, string.length() - 1).split(", ");
        }

        private ArrayValue toStringStore(String string) {
            return ArrayValue.stringStore(toStringArray(string));
        }

        public void clear() {
            if (monitor != null) {
                monitor.clear();
            }
        }

        @Override
        public void eventValue(ChannelTimeRecord ctr, Channel chnl) {
            dataType = ctr.getType();
            synchronized (lock) {
                lastValue = getObjectMap.get(dataType).get(ctr);
            }
            channelWrapper.liveValueProperty().setValue(ctr.stringValue());
        }

        @Override
        public void connectionMade(Channel chnl) {
            try {
                monitor = chnl.addMonitorValTime(this, 0);
                propertyListener = (ObservableValue<? extends String> ov, String t, String t1) -> {
                    synchronized (lock) {
                        if (!lastValue.toString().equals(t1)) {
                            try {
                                putObjectMap.get(dataType).put(t1);
                            } catch (ConnectionException | PutException ex) {
                                Logger.getLogger(ChannelSuiteWrapper.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                };
                channelWrapper.liveValueProperty().addListener(propertyListener);
                channelWrapper.connectedProperty().set(true);
            } catch (MonitorException ex) {
                Logger.getLogger(ChannelSuiteWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void connectionDropped(Channel chnl) {
            channelWrapper.liveValueProperty().removeListener(propertyListener);
            channelWrapper.liveValueProperty().setValue("DISCONNECTED");
            channelWrapper.connectedProperty().set(false);
        }
    }

    /**
     * Utility class to access SignalEntry private fields using reflections.
     */
    class SignalMap {

        private Map<String, Object> map;
        private ChannelSuite channelSuite;

        SignalMap(ChannelSuite channelSuite) {
            this.channelSuite = channelSuite;
            try {
                Field signalSuiteField = channelSuite.getClass().getDeclaredField("channelHandleMap");
                signalSuiteField.setAccessible(true);
                Object signalSuite = signalSuiteField.get(channelSuite);
                Field signalEntryField = signalSuite.getClass().getDeclaredField("signalMap");
                signalEntryField.setAccessible(true);
                map = (Map<String, Object>) signalEntryField.get(signalSuite);
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(ChannelSuiteWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        void setSignal(String handle, String signal) {
            try {
                Object signalEntry = map.get(handle);
                Field signalField = signalEntry.getClass().getDeclaredField("signal");
                signalField.setAccessible(true);
                signalField.set(signalEntry, signal);
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(ChannelSuiteWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }

            // Remove channel from cache to be able to restart monitors.
            try {
                Field channelMapField = channelSuite.getClass().getDeclaredField("channelHandleMap");
                channelMapField.setAccessible(true);
                Map<String, Channel> channelMap = (Map<String, Channel>) channelMapField.get(channelSuite);
                channelMap.remove(handle);
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(ChannelSuiteWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        void setValid(String handle, boolean valid) {
            try {
                Object signalEntry = map.get(handle);
                Field validField = signalEntry.getClass().getDeclaredField("valid");
                validField.setAccessible(true);
                validField.set(signalEntry, valid);
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(ChannelSuiteWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
