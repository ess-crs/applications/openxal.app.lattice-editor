/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import xal.extension.fxapplication.FxApplication;

/**
 * FXML Controller class
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class AcceleratorFilesController implements Initializable {

    private String acceleratorName;
    private String optics;
    private String simParams;
    private String deviceMapping;
    private String elementMapping;
    private String hardwareStatus;
    private String powerSupplies;
    private String timingManager;

    @FXML
    private TextField acceleratorNameTF;
    @FXML
    private TextField opticsTF;
    @FXML
    private TextField simParamsTF;
    @FXML
    private TextField deviceMappingTF;
    @FXML
    private TextField elementMappingTF;
    @FXML
    private TextField hardwareStatusTF;
    @FXML
    private TextField powerSuppliesTF;
    @FXML
    private TextField timingManagerTF;
    @FXML
    private Button okButton;
    @FXML
    private Button cancelButton;

    private Parent root;

    private boolean result = false;
    private String title = "Modify accelerator and file names";

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Do nothing.
    }

    public static AcceleratorFilesController createAcceleratorFilesDialog() {
        AcceleratorFilesController controller = null;

        FXMLLoader fxmlLoader = new FXMLLoader(FXMLController.class.getResource("/fxml/AcceleratorFiles.fxml"));
        try {
            Parent root = (Parent) fxmlLoader.load();
            controller = fxmlLoader.<AcceleratorFilesController>getController();
            controller.setRoot(root);
        } catch (IOException ex) {
            Logger.getLogger(AcceleratorFilesController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return controller;
    }

    public void setRoot(Parent root) {
        this.root = root;
    }

    public boolean show() {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle(title);
        stage.setScene(new Scene(root));
        FxApplication.setTheme(root.getScene());
        stage.showAndWait();

        return result;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        result = false;
        close();
    }

    @FXML
    private void okButtonAction(ActionEvent event) {
        if (!acceleratorNameTF.getText().isBlank()) {
            result = true;

            acceleratorName = acceleratorNameTF.getText().trim();
            optics = opticsTF.getText().trim();
            simParams = simParamsTF.getText().trim();
            deviceMapping = deviceMappingTF.getText().trim();
            elementMapping = elementMappingTF.getText().trim();
            hardwareStatus = hardwareStatusTF.getText().trim();
            powerSupplies = powerSuppliesTF.getText().trim();
            timingManager = timingManagerTF.getText().trim();

            close();
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Accelerator Name missing");
            alert.setHeaderText("Accelerator Name missing");
            alert.setContentText("Please set accelerator name.");

            alert.showAndWait();
        }
    }

    private void close() {
        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();
    }

    public String getAcceleratorName() {
        return acceleratorName;
    }

    public String getSimParams() {
        return "".equals(simParams) ? null : simParams;
    }

    public String getDeviceMapping() {
        return "".equals(deviceMapping) ? null : deviceMapping;
    }

    public String getElementMapping() {
        return "".equals(elementMapping) ? null : elementMapping;
    }

    public String getOptics() {
        return optics;
    }

    public String getHardwareStatus() {
        return "".equals(hardwareStatus) ? null : hardwareStatus;
    }

    public String getPowerSupplies() {
        return "".equals(powerSupplies) ? null : powerSupplies;
    }

    public String getTimingManager() {
        return "".equals(timingManager) ? null : timingManager;
    }

    public void setAcceleratorName(String acceleratorName) {
        this.acceleratorName = acceleratorName;
        acceleratorNameTF.setText(acceleratorName);

    }

    public void setOptics(String optics) {
        this.optics = optics;
        opticsTF.setText(optics != null ? optics : "");
    }

    public void setSimParams(String simParams) {
        this.simParams = simParams;
        simParamsTF.setText(simParams != null ? simParams : "");
    }

    public void setDeviceMapping(String deviceMapping) {
        this.deviceMapping = deviceMapping;
        deviceMappingTF.setText(deviceMapping != null ? deviceMapping : "");
    }

    public void setElementMapping(String elementMapping) {
        this.elementMapping = elementMapping;
        elementMappingTF.setText(elementMapping != null ? elementMapping : "");
    }

    public void setHardwareStatus(String hardwareStatus) {
        this.hardwareStatus = hardwareStatus;
        hardwareStatusTF.setText(hardwareStatus != null ? hardwareStatus : "");
    }

    public void setPowerSupplies(String powerSupplies) {
        this.powerSupplies = powerSupplies;
        powerSuppliesTF.setText(powerSupplies != null ? powerSupplies : "");
    }

    public void setTimingManager(String timingManager) {
        this.timingManager = timingManager;
        timingManagerTF.setText(timingManager != null ? timingManager : "");
    }

}
