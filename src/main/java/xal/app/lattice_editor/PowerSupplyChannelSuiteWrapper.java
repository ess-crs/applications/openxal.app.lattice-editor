/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import xal.smf.impl.MagnetPowerSupply;

/**
 * This class wraps around the Channel Suite of the
 * {@link xal.smf.impl.MagnetPowerSupply} object to provide JavaFX beans, which
 * can them be used by JavaFX applications.
 * <p>
 * The class also provides a method to represent the properties in a JavaFX
 * TableView.
 * <p>
 * Note: the monitors are cleared when a new ChannelSuiteWrapper instance is
 * created.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class PowerSupplyChannelSuiteWrapper extends ChannelSuiteWrapper {

    /**
     *
     * @param powerSupply
     */
    public PowerSupplyChannelSuiteWrapper(MagnetPowerSupply powerSupply) {
        clearMonitors();

        addChannelSuite(powerSupply.getChannelSuite());
    }
}
