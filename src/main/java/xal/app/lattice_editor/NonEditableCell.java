/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.util.Callback;

/**
 * Methods to create a TextFieldTableCell or a TextFieldTreeTableCell with the
 * CSS style "disabled-cell".
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class NonEditableCell {

    public static <S> Callback<TableColumn<S, String>, TableCell<S, String>> forTableColumn() {
        return (p) -> {
            TextFieldTableCell<S, String> cell = new TextFieldTableCell<>();
            cell.setEditable(false);
            cell.getStyleClass().add("disabled-cell");
            return cell;
        };
    }

    public static <S> Callback<TreeTableColumn<S, String>, TreeTableCell<S, String>> forTreeTableColumn() {
        return (p) -> {
            TextFieldTreeTableCell<S, String> cell = new TextFieldTreeTableCell<>();
            cell.setEditable(false);
            cell.getStyleClass().add("disabled-cell");
            return cell;
        };
    }
}
