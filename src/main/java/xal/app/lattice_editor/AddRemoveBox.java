/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

/**
 * This class creates an HBox with two buttons, one with a + icon and another
 * one with a - icon. Actions can be configured.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class AddRemoveBox extends HBox {

    private final Button addButton = new Button();
    private final Button removeButton = new Button();

    public AddRemoveBox() {
        this(null, null);
    }

    public AddRemoveBox(String addToolTip, String removeToolTip) {
        HBox.setHgrow(this, Priority.ALWAYS);
        HBox spacing = new HBox();
        HBox.setHgrow(spacing, Priority.ALWAYS);

        // Creating + and - buttons for elements
        Region addButtonRegion = new Region();
        addButtonRegion.setPrefSize(16, 16);
        addButtonRegion.getStyleClass().add("button-add");
        addButton.setGraphic(addButtonRegion);
        if (addToolTip != null) {
            addButton.setTooltip(new Tooltip(addToolTip));
        }

        Region removeButtonRegion = new Region();
        removeButtonRegion.setPrefSize(16, 16);
        removeButtonRegion.getStyleClass().add("button-remove");
        if (removeToolTip != null) {
            removeButton.setTooltip(new Tooltip(removeToolTip));
        }
        removeButton.setGraphic(removeButtonRegion);
        removeButton.setDisable(true);

        getChildren().addAll(spacing, addButton, removeButton);
    }

    public void setAddButtonAction(EventHandler<ActionEvent> action) {
        addButton.setOnAction(action);
    }

    public void setRemoveButtonAction(EventHandler<ActionEvent> action) {
        removeButton.setOnAction(action);
    }

    public void setDisableAddButton(boolean value) {
        addButton.setDisable(value);
    }

    public void setDisableRemoveButton(boolean value) {
        removeButton.setDisable(value);
    }
}
