/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import xal.tools.data.DataAttribute;
import xal.tools.data.DataListener;
import xal.tools.data.GenericRecord;
import xal.tools.data.InMemoryDataAdaptor;

/**
 * Generic wrapper that contains an undefined number of properties. It is
 * initialized from a GenericRecord or from a Collection of DataAttribute
 * objects (for default values).
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class SimParamWrapper {

    private Map<String, StringProperty> properties = new HashMap<>();
    private GenericRecord record;

    SimParamWrapper(Collection<DataAttribute> attributes) {
        for (DataAttribute attribute : attributes) {
            if (attribute.isPrimaryKey()) {
                setProperty(attribute.name(), "Default values");
            } else {
                String value = attribute.getDefaultValue() != null ? attribute.getDefaultValue().toString() : "";
                setProperty(attribute.name(), value);
            }
            setBindings(attribute);
        }
    }

    SimParamWrapper(GenericRecord record, Collection<String> attributes) {
        this.record = record;
        for (String attribute : attributes) {
            setProperty(attribute, record.valueForKey(attribute) != null ? record.valueForKey(attribute).toString() : "");
            setBindings(attribute, record);
        }
    }

    SimParamWrapper() {
    }

    public GenericRecord getRecord() {
        return record;
    }

    public Map<String, StringProperty> getProperties() {
        return properties;
    }

    public void setProperty(String name, String value) {
        if (properties.containsKey(name)) {
            properties.get(name).setValue(value);
        } else {
            properties.put(name, new SimpleStringProperty(value));
        }
    }

    public StringProperty getProperty(String name) {
        return properties.get(name);
    }

    /**
     * This method binds a GenericRecord with a property.
     *
     * @param attribute
     */
    private void setBindings(String attribute, GenericRecord record) {
        StringProperty property = getProperty(attribute);

        ChangeListener<String> cl = (ov, t, t1) -> {
            InMemoryDataAdaptor dataAdaptor = new InMemoryDataAdaptor("temp");
            record.write(dataAdaptor);
            dataAdaptor.setValue(attribute, t1);
            record.update(dataAdaptor);
        };

        property.addListener(cl);
    }

    /**
     * This method binds the default value of an attribute with a property.
     *
     * @param attribute
     */
    private void setBindings(DataAttribute attribute) {
        StringProperty property = getProperty(attribute.name());

        ChangeListener<String> cl = (ov, t, t1) -> {
            if ("".equals(t1.trim())) {
                try {
                    Field _strDVField = DataAttribute.class.getDeclaredField("DEFAULT_VALUE");
                    _strDVField.setAccessible(true);
                    _strDVField.set(attribute, null);
                } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(SimParamWrapper.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                InMemoryDataAdaptor dataAdaptor = new InMemoryDataAdaptor("temp");
                DataListener readerWriter = attribute.readerWriter();
                readerWriter.write(dataAdaptor);
                dataAdaptor.setValue("defaultValue", t1);
                readerWriter.update(dataAdaptor);
            }
        };

        property.addListener(cl);
    }
}
