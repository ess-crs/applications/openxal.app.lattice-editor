/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import xal.smf.AcceleratorNode;
import xal.smf.impl.MagnetPowerSupply;

/**
 * This class wraps around the main properties of the
 * {@link xal.smf.AcceleratorNode} object to provide JavaFX beans, which can
 * them be used by JavaFX applications.
 * <p>
 * The class also provides a method to represent the properties in a JavaFX
 * TableView.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class PowerSuppliesTableWrapper {

    private final ObservableList<PowerSupplyWrapper> powerSupplies = FXCollections.observableArrayList();

    /**
     * @param powerSupply
     */
    public PowerSuppliesTableWrapper(MagnetPowerSupply powerSupply) {
        for (AcceleratorNode node : powerSupply.getNodes()) {
            PowerSupplyWrapper powerSupplyWrapper = new PowerSupplyWrapper(node);
            powerSupplies.add(powerSupplyWrapper);
        }
    }

    public void setTableView(TableView table, TableColumn iconColumn, TableColumn nameColumn) {
        table.setEditable(true);
        table.getSelectionModel().cellSelectionEnabledProperty().set(true);
        iconColumn.setCellValueFactory(new PropertyValueFactory<>("icon"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        iconColumn.setCellFactory(c -> new ImageTableCell<>());
        nameColumn.setCellFactory(NonEditableCell.forTableColumn());
       
        table.itemsProperty().setValue(powerSupplies);
    }
}
