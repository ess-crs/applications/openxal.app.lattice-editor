/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.Stage;
import xal.ca.ChannelFactory;
import xal.extension.fxapplication.XalFxDocument;
import xal.sim.scenario.FileBasedElementMapping;
import xal.smf.Accelerator;
import xal.smf.data.XMLDataManager;

/**
 *
 * @author Juan F. Esteban Müller <juanf.estebanmuller@ess.eu>
 */
public class AppDocument extends XalFxDocument {

    AppDocument(Stage stage) {
        super(stage);

        DEFAULT_FILENAME = "main.xal";

        // Initialise variables FILETYPE_DESCRIPTION, WILDCARD_FILE_EXTENSION,
        // DEFAULT_FILENAME, and HELP_PAGEID (see xal.extension.fxapplication.XalFxDocument)
    }

    /**
     * Save the ScannerDocument document to the specified URL.
     *
     * @param url The file URL where the data should be saved
     */
    @Override
    public void saveDocumentAs(URL url) {
        // Not required.
    }

    /**
     * Reads the content of the document from the specified URL, and loads the
     * information into the application.
     *
     * @param url The path to the XML file
     */
    @Override
    public void loadDocument(URL url) {
        // Using load accelerator instead by overriding loadFileMenuHandler() in MainApp
    }

    @Override
    public void newDocument() {
        source = null;

        AcceleratorFilesController dialog = AcceleratorFilesController.createAcceleratorFilesDialog();
        dialog.setTitle("Create new accelerator");

        boolean result = dialog.show();
        if (result) {

            Accelerator newAccelerator = new Accelerator(dialog.getAcceleratorName());
            newAccelerator.setElementMapping(new FileBasedElementMapping(newAccelerator.getElementMapping()));
            accelerator.setAccelerator(newAccelerator);

            acceleratorXMLManager = XMLDataManager.newEmptyManager(ChannelFactory.defaultFactory());

            acceleratorXMLManager.setDeviceMappingUrlSpec(dialog.getDeviceMapping());
            acceleratorXMLManager.setElementMappingUrlSpec(dialog.getElementMapping());
            acceleratorXMLManager.setOpticsUrlSpec(dialog.getOptics());
            acceleratorXMLManager.setTimingManagerUrlSpec(dialog.getTimingManager());
            acceleratorXMLManager.setHardwareStatusUrlSpec(dialog.getHardwareStatus());
            acceleratorXMLManager.setPowerSuppliesUrlSpec(dialog.getPowerSupplies());
            acceleratorXMLManager.setUrlSpecForTableGroup(dialog.getSimParams(), "modelparams");
        }
    }

    @Override
    public void saveDocument() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Saving accelerator to {0}", sourceString.getValue());
        // Save to file
        getAcceleratorXMLManager().setMainPath(sourceString.getValue());
        getAcceleratorXMLManager().writeAccelerator(getAccelerator());
        // Load the accelerator.
        setAcceleratorXMLManager(XMLDataManager.managerWithFilePath(sourceString.getValue()));
    }
}
