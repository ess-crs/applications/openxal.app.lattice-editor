/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.util.Callback;
import xal.smf.AcceleratorNode;
import xal.smf.attr.AttributeBucket;
import xal.smf.attr.TwissBucket;

/**
 * This class wraps around the attribute buckets of the
 * {@link xal.smf.AcceleratorNode} object to provide JavaFX beans, which can
 * them be used by JavaFX applications.
 * <p>
 * The class also provides a method to represent the properties in a JavaFX
 * TreeTableView.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class AttributesTableWrapper {

    private final TreeItem<AttributeWrapper> attributes = new TreeItem<>(new AttributeWrapper("", "", ""));

    /**
     *
     * @param node
     */
    public AttributesTableWrapper(AcceleratorNode node) {
        for (AttributeBucket bucket : node.getBuckets()) {
            if (!(bucket instanceof TwissBucket)) {
                TreeItem<AttributeWrapper> attributeBucketItem = new TreeItem<>(new AttributeWrapper(bucket.getType(), "", ""));
                for (String attribute : bucket.getAttrNames()) {
                    AttributeWrapper attributeWrapper = new AttributeWrapper(attribute, bucket.getAttr(attribute).stringValue(), bucket.getAttrDescription(attribute));
                    attributeBucketItem.getChildren().add(new TreeItem<>(attributeWrapper));

                    // Bind value property to attribute.
                    attributeWrapper.valueProperty().addListener((ChangeListener<String>) (ov, t, t1) -> bucket.parseAttrValue(attribute, t1));
                }
                attributeBucketItem.setExpanded(true);
                attributes.getChildren().add(attributeBucketItem);
            }
        }
    }

    public void setTreeTableView(TreeTableView table, TreeTableColumn nameColumn, TreeTableColumn valueColumn, TreeTableColumn descriptionColumn) {
        table.setRoot(attributes);
        table.setShowRoot(false);

        table.setEditable(true);
        table.getSelectionModel().cellSelectionEnabledProperty().set(true);

        nameColumn.setCellFactory(NonEditableCell.forTreeTableColumn());
        valueColumn.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());
        descriptionColumn.setCellFactory(NonEditableCell.forTreeTableColumn());

        nameColumn.setCellValueFactory((Callback<CellDataFeatures<AttributeWrapper, String>, ObservableValue<String>>) (CellDataFeatures<AttributeWrapper, String> p) -> p.getValue().getValue().nameProperty());
        valueColumn.setCellValueFactory((Callback<CellDataFeatures<AttributeWrapper, String>, ObservableValue<String>>) (CellDataFeatures<AttributeWrapper, String> p) -> p.getValue().getValue().valueProperty());
        descriptionColumn.setCellValueFactory((Callback<CellDataFeatures<AttributeWrapper, String>, ObservableValue<String>>) (CellDataFeatures<AttributeWrapper, String> p) -> p.getValue().getValue().descriptionProperty());
    }
}
