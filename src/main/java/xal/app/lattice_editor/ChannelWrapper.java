/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class ChannelWrapper {

    private StringProperty handle = new SimpleStringProperty("–");

    private StringProperty signal = new SimpleStringProperty("–");
    private BooleanProperty valid = new SimpleBooleanProperty(true);
    private BooleanProperty settable = new SimpleBooleanProperty(true);
    private StringProperty liveValue = new SimpleStringProperty();
    private BooleanProperty connected = new SimpleBooleanProperty(false);

    ChannelWrapper(String handle, String signal) {
        this(handle, signal, true, true);
    }

    ChannelWrapper(String handle, String signal, boolean valid, boolean settable) {
        this.handle.setValue(handle);
        this.signal.setValue(signal);
        this.valid.setValue(valid);
        this.settable.setValue(settable);
        this.liveValue.setValue("");
    }

    public String getHandle() {
        return handle.getValue();
    }

    public String getSignal() {
        return signal.getValue();
    }

    public Boolean getValid() {
        return valid.getValue();
    }

    public Boolean getSettable() {
        return settable.getValue();
    }

    public String getLiveValue() {
        return liveValue.getValue();
    }

    public boolean isConnected() {
        return connected.getValue();
    }

    public StringProperty handleProperty() {
        return handle;
    }

    public StringProperty signalProperty() {
        return signal;
    }

    public BooleanProperty validProperty() {
        return valid;
    }

    public BooleanProperty settableProperty() {
        return settable;
    }

    public StringProperty liveValueProperty() {
        return liveValue;
    }

    public BooleanProperty connectedProperty() {
        return connected;
    }
}
