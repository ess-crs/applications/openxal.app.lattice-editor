/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import xal.extension.fxapplication.FxApplication;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNodeFactory;

/**
 * This class wraps around the {@link xal.smf.AcceleratorNodeFactory} object to
 * provide JavaFX beans, which can them be used by JavaFX applications.
 * <p>
 * The class also provides a method to represent the properties in a JavaFX
 * TableView and methods to add and remove device types.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class DeviceManagerTableWrapper {

    private final ObservableList<DeviceTypeWrapper> deviceTypes = FXCollections.observableArrayList();
    final AcceleratorNodeFactory nodeFactory;
    private Map<String, Class<?>> classTable = null;
    private Map<String, Constructor<? extends Object>> constructors = null;

    public DeviceManagerTableWrapper(Accelerator accelerator) {
        nodeFactory = accelerator.getNodeFactory();
        Field ctField = null;
        Field cField = null;
        try {
            ctField = AcceleratorNodeFactory.class.getDeclaredField("classTable");
            ctField.setAccessible(true);
            cField = AcceleratorNodeFactory.class.getDeclaredField("constructors");
            cField.setAccessible(true);

        } catch (NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            classTable = (Map<String, Class<?>>) ctField.get(nodeFactory);
            constructors = (Map<String, Constructor<? extends Object>>) cField.get(nodeFactory);
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(DeviceManagerTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (String type : classTable.keySet()) {
            Class cls = classTable.get(type);
            String softType = "";
            if (type.contains(".")) {
                softType = type.substring(type.indexOf('.') + 1);
                type = type.substring(0, type.indexOf('.'));
            }
            DeviceTypeWrapper newType = new DeviceTypeWrapper(type, softType, cls, nodeFactory);
            deviceTypes.add(newType);
            setBindings(newType);
        }
    }

    private void setBindings(DeviceTypeWrapper deviceTypeWrapper) {
        /* Bindings */
        // Update type in AcceleratorNodeFactory.
        deviceTypeWrapper.type.addListener((ChangeListener<String>) (ov, oldType, newType) -> {
            String oldNodeType = deviceTypeWrapper.getSoftType().equals("") ? oldType : oldType + "." + deviceTypeWrapper.getSoftType();
            try {
                this.nodeFactory.registerNodeClass(newType, deviceTypeWrapper.getSoftType().equals("") ? null : deviceTypeWrapper.getSoftType(), (Class) Class.forName(deviceTypeWrapper.getClassName()));
                this.classTable.remove(oldNodeType);
                this.constructors.remove(oldNodeType);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DeviceTypeWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        // Update soft type in AcceleratorNodeFactory.
        deviceTypeWrapper.softType.addListener((ChangeListener<String>) (ov, oldSoftType, newSoftType) -> {
            String oldNodeType = oldSoftType.equals("") ? deviceTypeWrapper.getType() : deviceTypeWrapper.getType() + "." + oldSoftType;
            try {
                this.nodeFactory.registerNodeClass(deviceTypeWrapper.getType(), newSoftType.equals("") ? null : newSoftType, (Class) Class.forName(deviceTypeWrapper.getClassName()));
                this.classTable.remove(oldNodeType);
                this.constructors.remove(oldNodeType);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DeviceTypeWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        // Update AcceleratorNodeFactory when a valid class is entered.
        deviceTypeWrapper.className.addListener((ChangeListener<String>) (ov, oldClassName, newClassName) -> {
            String nodeType = deviceTypeWrapper.getSoftType().equals("") ? deviceTypeWrapper.getType() : deviceTypeWrapper.getType() + "." + deviceTypeWrapper.getSoftType();

            try {
                Class newCls = Class.forName(newClassName);
                this.classTable.remove(nodeType);
                this.constructors.remove(nodeType);
                this.nodeFactory.registerNodeClass(deviceTypeWrapper.getType(), deviceTypeWrapper.getSoftType().equals("") ? null : deviceTypeWrapper.getSoftType(), newCls);
                deviceTypeWrapper.setError(false);
            } catch (ClassNotFoundException ex) {
                deviceTypeWrapper.setError(true);
            }
        });
    }

    public List<String> getTypesForClass(Class cls) {
        List<String> types = new ArrayList<>();

        for (DeviceTypeWrapper dt : deviceTypes) {
            if (dt.getClassName().equals(cls.getCanonicalName())) {
                types.add(dt.getType());
            }
        }

        return types;
    }

    public Class getClassForTypes(String type) {
        return classTable.get(type);
    }

    public List<String> getSoftTypesForType(String type) {
        List<String> softTypes = new ArrayList<>();

        for (DeviceTypeWrapper dt : deviceTypes) {
            if (dt.getType().equals(type)) {
                softTypes.add(dt.getSoftType());
            }
        }

        return softTypes;
    }

    public void setTableView(TableView table, TableColumn iconColumn, TableColumn typeColumn, TableColumn softTypeColumn, TableColumn classNameColumn) {
        table.itemsProperty().setValue(deviceTypes);

        table.setEditable(true);

        iconColumn.setCellValueFactory(new PropertyValueFactory<>("icon"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        softTypeColumn.setCellValueFactory(new PropertyValueFactory<>("softType"));
        classNameColumn.setCellValueFactory(new PropertyValueFactory<>("className"));

        iconColumn.setCellFactory(c -> new ImageTableCell<>());
        typeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        softTypeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        classNameColumn.setCellFactory(c -> new ErrorStyleTableCell<DeviceTypeWrapper>());

    }

    public void addDeviceType() {
        AddDeviceTypeDialog dialog = new AddDeviceTypeDialog();
        Optional<DeviceTypeWrapper> result = dialog.showAndWait();

        result.ifPresent(newType -> {
            try {
                nodeFactory.registerNodeClass(newType.typeProperty().getValue(), newType.softTypeProperty().getValue(), (Class) Class.forName(newType.classNameProperty().getValue()));
                deviceTypes.add(newType);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DeviceManagerTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void removeDeviceType(String type, String softType) {
        for (DeviceTypeWrapper dtw : deviceTypes) {
            if (dtw.typeProperty().getValue().equals(type) && dtw.softTypeProperty().getValue().equals(softType)) {
                deviceTypes.remove(dtw);
                break;
            }
        }
        String nodeType = softType.equals("") ? type : type + "." + softType;
        classTable.remove(nodeType);
        constructors.remove(nodeType);
    }

    class AddDeviceTypeDialog extends Dialog<DeviceTypeWrapper> {

        private final PseudoClass ERROR = PseudoClass.getPseudoClass("error");

        private TextField typeField;
        private TextField softTypeField;
        private TextField classField;
        Label typeError;
        Label classError;

        AddDeviceTypeDialog() {
            FxApplication.setTheme(getDialogPane().getScene());

            setTitle("Add a new Device Type");
            setHeaderText("Add a new Device Type");

            // Set the button types.
            getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

            // Create the text fields.
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 10, 10, 10));

            typeField = new TextField();
            typeField.setPromptText("Type");
            softTypeField = new TextField();
            softTypeField.setPromptText("Soft Type (optional)");
            classField = new TextField();
            classField.setPromptText("Class name");

            typeError = new Label();
            typeError.setPrefWidth(150);
            typeError.setText("");
            classError = new Label();
            classError.setPrefWidth(150);
            classError.setText("");

            grid.add(new Label("Type:"), 0, 0);
            grid.add(typeField, 1, 0);
            grid.add(typeError, 2, 0);
            grid.add(new Label("Soft Type:"), 0, 1);
            grid.add(softTypeField, 1, 1);
            grid.add(new Label("Class name:"), 0, 2);
            grid.add(classField, 1, 2);
            grid.add(classError, 2, 2);

            // Enable/Disable OK button depending on whether a type and class name were entered.
            Node okButton = getDialogPane().lookupButton(ButtonType.OK);
            okButton.setDisable(true);

            // Do some validation.
            typeField.textProperty().addListener((observable, oldValue, newValue) -> {
                boolean error = validateType(newValue.trim(), softTypeField.getText().trim());
                typeField.pseudoClassStateChanged(ERROR, error);
                softTypeField.pseudoClassStateChanged(ERROR, error);

                error |= validateClass(classField.getText().trim());

                okButton.setDisable(error);
            });

            softTypeField.textProperty().addListener((observable, oldValue, newValue) -> {
                boolean error = validateType(typeField.getText().trim(), newValue.trim());
                typeField.pseudoClassStateChanged(ERROR, error);
                softTypeField.pseudoClassStateChanged(ERROR, error);

                error |= validateClass(classField.getText().trim());
                okButton.setDisable(error);
            });

            classField.textProperty().addListener((observable, oldValue, newValue) -> {
                boolean error = validateClass(newValue.trim());
                classField.pseudoClassStateChanged(ERROR, error);

                error |= validateType(typeField.getText().trim(), softTypeField.getText().trim());

                okButton.setDisable(error);
            });

            getDialogPane().setContent(grid);

            // Request focus on the type field by default.
            Platform.runLater(() -> typeField.requestFocus());

            // Convert the result to a DeviceTypeWrapper when the OK button is clicked.
            setResultConverter(dialogButton -> {
                if (dialogButton == ButtonType.OK) {
                    try {
                        return new DeviceTypeWrapper(typeField.getText().trim(), softTypeField.getText().trim(), Class.forName(classField.getText().trim()), DeviceManagerTableWrapper.this.nodeFactory);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(DeviceManagerTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return null;
            });
        }

        private boolean validateType(String type, String softType) {
            boolean error = true;
            String message = "";

            if (!type.equals("")) {
                String nodeType = softType.equals("") ? type : type + "." + softType;
                if (classTable.containsKey(nodeType)) {
                    message = "Type already exists.";
                } else {
                    error = false;
                }
            }

            typeError.setText(message);
            return error;
        }

        private boolean validateClass(String className) {
            boolean error = false;
            String message = "";

            try {
                Class cls = Class.forName(className);
            } catch (ClassNotFoundException ex) {
                error = true;
                message = "Class not found.";
            }

            classError.setText(message);
            return error;
        }
    }
}
