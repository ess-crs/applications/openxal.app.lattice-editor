/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.lattice_editor;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import xal.extension.fxapplication.Controller;
import xal.extension.fxapplication.widgets.AcceleratorTreeView;
import xal.extension.fxapplication.widgets.ComboSequencesTreeView;
import xal.extension.fxapplication.widgets.PowerSuppliesTreeView;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;
import xal.smf.AcceleratorSeqCombo;
import xal.smf.impl.MagnetMainSupply;
import xal.smf.impl.MagnetPowerSupply;

/**
 * FXML Controller class
 *
 * @author Juan F. Esteban Müller <juanf.estebanmuller@ess.eu>
 */
public class FXMLController extends Controller {

    private AcceleratorTreeView modelTreeView;
    private AcceleratorTreeView firstSequenceTreeView;
    private AcceleratorTreeView lastSequenceTreeView;
    private ComboSequencesTreeView comboTreeView;
    private PowerSuppliesTreeView powerSuppliesTreeView;

    @FXML
    private SplitPane treePane;

    @FXML
    private TreeTableColumn<String, String> ttcAttribute;
    @FXML
    private TreeTableColumn<?, ?> ttcValue;
    @FXML
    private Tab tabDesign;
    @FXML
    private TableView<?> propertiesTV;
    @FXML
    private TableView<?> epicsTV;
    @FXML
    private TitledPane propertiesTP;
    @FXML
    private TableColumn<?, ?> tcProperty;
    @FXML
    private TableColumn<?, ?> tcValue;
    @FXML
    private TreeTableColumn<?, ?> ttcDescription;
    @FXML
    private TableColumn<?, ?> tcHandle;
    @FXML
    private TableColumn<?, ?> tcSignal;
    @FXML
    private TableColumn<?, ?> tcSettable;
    @FXML
    private TableColumn<?, ?> tcValid;
    @FXML
    private TableColumn<?, ?> tcLiveValue;
    @FXML
    private TitledPane attributesTP;
    @FXML
    private TreeTableView<?> attributesTTV;
    @FXML
    private TitledPane epicsTP;
    @FXML
    private SplitPane latticeSP;

    boolean areAllFiltersSelected;
    private String[] selectedFilters;
    @FXML
    private Tab comboSequencesTab;
    @FXML
    private TabPane tabPane;
    @FXML
    private GridPane comboSequencesPane;
    @FXML
    private Button saveComboB;
    @FXML
    private TextField comboNameTF;

    private AddRemoveBox addRemoveElementBox;
    private AddRemoveBox addRemoveComboBox;
    private AddRemoveBox addRemovePSBox;
    @FXML
    private Label comboSeqDetails;

    @FXML
    private TableColumn<?, ?> iconColumnDM;
    @FXML
    private TableColumn<DeviceTypeWrapper, String> typeColumDM;
    @FXML
    private TableColumn<?, ?> softTypeColumDM;
    @FXML
    private TableColumn<?, ?> classColumnDM;
    @FXML
    private TableView<DeviceTypeWrapper> deviceManagerTV;
    private boolean showingOnlySequences = false;

    private DeviceManagerTableWrapper deviceManagerTableWrapper;
    @FXML
    private Button removeDTButton;
    @FXML
    private Tab tabSimParam;
    @FXML
    private Tab deviceMappingTab;
    @FXML
    private Tab modelMappingTab;
    @FXML
    private TableView<?> defaultTypesTV;
    @FXML
    private TableColumn<?, ?> iconColumnDefMM;
    @FXML
    private TableColumn<?, ?> typeColumDefMM;
    @FXML
    private TableColumn<?, ?> classColumnDefMM;
    @FXML
    private TableView<ElementModelWrapper> modelManagerTV;
    @FXML
    private TableColumn<?, ?> iconColumnMM;
    @FXML
    private TableColumn<ElementModelWrapper, String> typeColumMM;
    @FXML
    private TableColumn<?, ?> classColumnMM;
    @FXML
    private Button removeETButton;

    private ElementMappingTableWrapper elementMappingTableWrapper;
    @FXML
    private VBox simVBox;
    private SimParamsTableWrapper simParamsTableWrapper;
    @FXML
    private Tab tabPowerSupplies;
    @FXML
    private TextField powerSupplyTF;
    @FXML
    private TitledPane epicsTPPS;
    @FXML
    private TableView<?> epicsTVPS;
    @FXML
    private TableColumn<?, ?> tcHandlePS;
    @FXML
    private TableColumn<?, ?> tcSignalPS;
    @FXML
    private TableColumn<?, ?> tcSettablePS;
    @FXML
    private TableColumn<?, ?> tcValidPS;
    @FXML
    private TableColumn<?, ?> tcLiveValuePS;
    @FXML
    private TableView<?> powerSuppliesTV;
    @FXML
    private TableColumn<?, ?> iconColumnPS;
    @FXML
    private TableColumn<?, ?> elementColumnPS;

    private ChangeListener<String> powerSupplyListener;

    private ObservableList<String> powerSuppliesList = FXCollections.observableArrayList();

//    @FXML
//    private Button addSimParamsB;
//    @FXML
//    private Button removeSimParamsB;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Setting up the AcceleratorTreeView
        modelTreeView = new AcceleratorTreeView();
        comboTreeView = new ComboSequencesTreeView();
        modelTreeView.disableDefaultClickEventHandler();
        powerSuppliesTreeView = new PowerSuppliesTreeView();

        treePane.getItems().addAll(modelTreeView);

        // Setting actions for changes in selected item in tree views.
        modelTreeView.selectedItemProperty().addListener((ChangeListener<TreeItem<AcceleratorNode>>) (ov, t, t1) -> itemSelectionChanged(t1));
        comboTreeView.selectedItemProperty().addListener((ChangeListener<TreeItem<AcceleratorNode>>) (ov, t, t1) -> loadComboSequence(t1));
        powerSuppliesTreeView.selectedItemProperty().addListener((ChangeListener<TreeItem<MagnetPowerSupply>>) (ov, t, t1) -> itemSelectionChangedPS(t1));

        // Setting action for tab change.
        tabPane.getSelectionModel().selectedItemProperty().addListener((ChangeListener<Tab>) (ov, t, t1) -> tabSelected(t1));

        // Creating + and - buttons for elements when Designtab is shown.
        addRemoveElementBox = new AddRemoveBox("Add a new element", "Remove the element selected");
        addRemoveElementBox.setAddButtonAction((event) -> addNewElement());
        addRemoveElementBox.setRemoveButtonAction((event) -> removeElement());

        modelTreeView.getBottombar().getChildren().add(addRemoveElementBox);

        // Generating Combo Sequence tab.
        firstSequenceTreeView = new AcceleratorTreeView();
        firstSequenceTreeView.disableDefaultClickEventHandler();
        lastSequenceTreeView = new AcceleratorTreeView();
        lastSequenceTreeView.disableDefaultClickEventHandler();
        comboSequencesPane.add(firstSequenceTreeView, 0, 1);
        comboSequencesPane.add(lastSequenceTreeView, 1, 1);

        firstSequenceTreeView.addSingleClickEventHandler((e) -> calculateComboLenght());
        lastSequenceTreeView.addSingleClickEventHandler((e) -> calculateComboLenght());

        // Creating + and - buttons for comboSequencesPane when Combo sequence tab is shown.
        addRemoveComboBox = new AddRemoveBox("Add a new Combo sequence", "Remove the selected Combo sequence");
        addRemoveComboBox.setAddButtonAction((event) -> addNewCombo());
        addRemoveComboBox.setRemoveButtonAction((event) -> removeCombo());

        // Creating + and - buttons for power supplies
        addRemovePSBox = new AddRemoveBox("Add a new power supply", "Remove the power supply selected");
        addRemovePSBox.setAddButtonAction((event) -> addNewPowerSupply());
        addRemovePSBox.setRemoveButtonAction((event) -> removePowerSupply());
        powerSuppliesTreeView.getBottombar().getChildren().add(addRemovePSBox);

        removeDTButton.setDisable(true);
        deviceManagerTV.getSelectionModel().selectedItemProperty().addListener((ChangeListener<? super DeviceTypeWrapper>) (ov, t, t1) -> removeDTButton.setDisable(t1 == null));
        removeETButton.setDisable(true);
        modelManagerTV.getSelectionModel().selectedItemProperty().addListener((ChangeListener<? super ElementModelWrapper>) (ov, t, t1) -> removeETButton.setDisable(t1 == null));

        simParamsTableWrapper = new SimParamsTableWrapper(simVBox);
    }

    @Override
    public void beforeStart() {
        modelTreeView.setDocument(getApplication().getDocument());
        comboTreeView.setDocument(getApplication().getDocument());
        powerSuppliesTreeView.setDocument(getApplication().getDocument());

        firstSequenceTreeView.setDocument(getApplication().getDocument());
        firstSequenceTreeView.hideFilterMenu();
        firstSequenceTreeView.deselectAllFilters();
        firstSequenceTreeView.setAlwaysShowRfCavities(true);

        lastSequenceTreeView.setDocument(getApplication().getDocument());
        lastSequenceTreeView.hideFilterMenu();
        lastSequenceTreeView.deselectAllFilters();
        lastSequenceTreeView.setAlwaysShowRfCavities(true);

        areAllFiltersSelected = modelTreeView.areAllFiltersSelected();
        selectedFilters = modelTreeView.getSelectedFilters();

        simParamsTableWrapper.loadEditContext(getApplication().getDocument().getAccelerator().editContext());

        getApplication().getDocument().getAcceleratorProperty().addChangeListener((ChangeListener<Accelerator>) (ov, t, newAccelerator) -> {
            updateDM(newAccelerator);
            updateMM(newAccelerator);
            simParamsTableWrapper.loadEditContext(newAccelerator.editContext());
            updatePowerSupplies(newAccelerator);
        });

        try {
            updateDM(getApplication().getDocument().getAccelerator());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            updateMM(getApplication().getDocument().getAccelerator());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        updatePowerSupplies(getApplication().getDocument().getAccelerator());
    }

    /**
     * Handles all changes to be done in the UI when changing tab. In
     * particular, showing the corresponding left panel for the tab, and loading
     * the right information in the visible tab.
     *
     * @param event
     */
    private void tabSelected(Tab tab) {
        if (tab != comboSequencesTab) {
            comboTreeView.getBottombar().getChildren().remove(addRemoveComboBox);
        }

        if (tab == tabDesign) {
            modelTreeView.getBottombar().getChildren().add(addRemoveElementBox);
            treePane.getItems().clear();
            treePane.getItems().addAll(modelTreeView);
            showSequencesAndElements();
            itemSelectionChangedDesign(modelTreeView.getSelectedItem());
        } else {
            modelTreeView.getBottombar().getChildren().remove(addRemoveElementBox);
        }

        if (tab == tabPowerSupplies) {
            treePane.getItems().clear();
            treePane.getItems().addAll(powerSuppliesTreeView);
            itemSelectionChangedPS(powerSuppliesTreeView.getSelectedItem());
        }

        if (tab == tabSimParam) {
            showOnlySequences();
            treePane.getItems().clear();
            treePane.getItems().addAll(modelTreeView, comboTreeView);
        }

        if (tab == comboSequencesTab) {
            comboTreeView.getBottombar().getChildren().add(addRemoveComboBox);
            treePane.getItems().clear();
            treePane.getItems().addAll(comboTreeView);
            if (comboTreeView.getSelectedNodeId() == null) {
                comboNameTF.setText("");
                firstSequenceTreeView.clearSelection();
                lastSequenceTreeView.clearSelection();
                firstSequenceTreeView.setDisable(true);
                lastSequenceTreeView.setDisable(true);
                comboNameTF.setDisable(true);
                saveComboB.setDisable(true);
                addRemoveComboBox.setDisableRemoveButton(true);
            } else {
                addRemoveComboBox.setDisableRemoveButton(false);
            }
        } else {
            comboTreeView.getBottombar().getChildren().remove(addRemoveComboBox);
        }

        if (tab == deviceMappingTab) {
            treePane.getItems().clear();
            treePane.getItems().addAll(modelTreeView);
            showSequencesAndElements();
            itemSelectionChangedDM(modelTreeView.getSelectedItem());
        }

        if (tab == modelMappingTab) {
            treePane.getItems().clear();
            treePane.getItems().addAll(modelTreeView);
            showSequencesAndElements();
            itemSelectionChangedMM(modelTreeView.getSelectedItem());
        }
    }


    /*
     *********************************************
     * Handle selection changed on ModelTreeView *
     *********************************************
     */
    private void itemSelectionChanged(TreeItem<AcceleratorNode> item) {
        if (tabDesign.isSelected()) {
            itemSelectionChangedDesign(item);
        }
        if (deviceMappingTab.isSelected()) {
            itemSelectionChangedDM(item);
        }
        if (modelMappingTab.isSelected()) {
            itemSelectionChangedMM(item);
        }
    }

    private void itemSelectionChangedDesign(TreeItem<AcceleratorNode> item) {
        if (item != null) {
            AcceleratorNode node = item.getValue();

            PropertiesTableWrapper propertiesTable = new PropertiesTableWrapper(node, deviceManagerTableWrapper, powerSuppliesList);
            propertiesTable.setTableView(propertiesTV, tcProperty, tcValue);
            propertiesTable.addNodeChangedListener((nodeBefore, nodeAfter) -> {
                if (nodeBefore != nodeAfter) {
                    modelTreeView.replaceElement(nodeBefore, nodeAfter);
                }
                modelTreeView.refresh();
            });

            AttributesTableWrapper attributesTableWrapper = new AttributesTableWrapper(node);
            attributesTableWrapper.setTreeTableView(attributesTTV, ttcAttribute, ttcValue, ttcDescription);

            ChannelSuiteWrapper channels = new ChannelSuiteWrapper(node);
            channels.setTableView(epicsTV, tcHandle, tcSignal, tcValid, tcSettable, tcLiveValue);

            propertiesTable.addPSChangedListener((nodeBefore, nodeAfter) -> {
                ChannelSuiteWrapper channelSuite = new ChannelSuiteWrapper(nodeAfter);
                channelSuite.setTableView(epicsTV, tcHandle, tcSignal, tcValid, tcSettable, tcLiveValue);
            });

            addRemoveElementBox.setDisableRemoveButton(false);
        } else {
            propertiesTV.getItems().clear();
            if (attributesTTV.getRoot() != null) {
                attributesTTV.getRoot().getChildren().clear();
            }
            epicsTV.getItems().clear();
            addRemoveElementBox.setDisableRemoveButton(true);
        }
    }

    private void itemSelectionChangedDM(TreeItem<AcceleratorNode> item) {
        // Select device type. TODO: Scroll to see row.
        deviceManagerTV.getSelectionModel().clearSelection();
        if (item != null) {
            for (DeviceTypeWrapper row : deviceManagerTV.getItems()) {
                String deviceType = typeColumDM.getCellObservableValue(row).getValue();
                if (deviceType.equals(item.getValue().getType())) {
                    deviceManagerTV.getSelectionModel().select(row);
                }
            }
        }
    }

    private void itemSelectionChangedMM(TreeItem<AcceleratorNode> item) {
        // Select element type. TODO: Scroll to see row.
        if (item != null) {
            modelManagerTV.getSelectionModel().clearSelection();
            for (ElementModelWrapper row : modelManagerTV.getItems()) {
                String elementType = typeColumMM.getCellObservableValue(row).getValue();
                if (elementType.equalsIgnoreCase(item.getValue().getType())) {
                    modelManagerTV.getSelectionModel().select(row);
                }
            }
        }
    }

    private void itemSelectionChangedPS(TreeItem<MagnetPowerSupply> item) {
        if (item != null) {
            MagnetPowerSupply powerSupply = item.getValue();

            if (powerSupplyListener != null) {
                powerSupplyTF.textProperty().removeListener(powerSupplyListener);
                powerSupplyListener = null;
            }

            if (powerSupply != null) {
                addRemovePSBox.setDisableRemoveButton(false);
                powerSupplyListener = (ov, t, t1) -> {
                    powerSupply.setId(t1);
                    powerSuppliesTreeView.refresh();
                };
                powerSupplyTF.setText(powerSupply.getId());
                powerSupplyTF.textProperty().addListener(powerSupplyListener);
                ChannelSuiteWrapper channels = new PowerSupplyChannelSuiteWrapper(powerSupply);
                channels.setTableView(epicsTVPS, tcHandlePS, tcSignalPS, tcValidPS, tcSettablePS, tcLiveValuePS);
                PowerSuppliesTableWrapper powerSuppliesTableWrapper = new PowerSuppliesTableWrapper(powerSupply);
                powerSuppliesTableWrapper.setTableView(powerSuppliesTV, iconColumnPS, elementColumnPS);
            } else {
                addRemovePSBox.setDisableRemoveButton(true);
                powerSupplyTF.setText("");
                powerSupplyListener = null;
                epicsTV.getItems().clear();
                powerSuppliesTV.getItems().clear();
            }
        }
    }

    /*
     *******************************
     * Design Tab related methods. *
     *******************************
     */
    private void addNewElement() {
        Accelerator accelerator = getApplication().getDocument().getAccelerator();
        NewElementController dialog = NewElementController.createNewElementDialog(accelerator, deviceManagerTableWrapper);
        String nodeId = dialog.show();
        if (nodeId != null) {
            modelTreeView.addElement(accelerator.getNodeWithId(nodeId));
            // Make sure the treeview is updated before selectElement is called.
            Platform.runLater(() -> modelTreeView.selectElement(nodeId));
        }
    }

    private void removeElement() {
        TreeItem<AcceleratorNode> item = modelTreeView.getSelectedItem();
        if (item != null && item.getParent() != null) {
            item.getParent().getChildren().remove(item);
            modelTreeView.refresh();
            AcceleratorNode node = item.getValue();
            if (node != null && node.getParent() != null) {
                node.getParent().removeNode(node);
                itemSelectionChangedDesign(modelTreeView.getSelectedItem());
            }
        }
    }

    private void showOnlySequences() {
        if (!showingOnlySequences) {
            showingOnlySequences = true;
            areAllFiltersSelected = modelTreeView.areAllFiltersSelected();
            selectedFilters = modelTreeView.getSelectedFilters();
            modelTreeView.hideFilterMenu();
            modelTreeView.deselectAllFilters();
            modelTreeView.setAlwaysShowRfCavities(true);
        }
    }

    private void showSequencesAndElements() {
        if (showingOnlySequences) {
            showingOnlySequences = false;
            modelTreeView.setAlwaysShowRfCavities(false);
            if (areAllFiltersSelected) {
                modelTreeView.selectAllFilters();
            } else {
                modelTreeView.selectFilters(selectedFilters);
            }
            modelTreeView.showFilterMenu();
        }
    }

    /*
     ****************************************
     * Combo Sequences Tab related methods. *
     ****************************************
     */
    /**
     * Called when clicked on a combo sequence in the combotreeview
     *
     * @param item
     */
    private void loadComboSequence(TreeItem<AcceleratorNode> item) {
        if (item == null) {
            addRemoveComboBox.setDisableRemoveButton(true);
            firstSequenceTreeView.clearSelection();
            lastSequenceTreeView.clearSelection();
            firstSequenceTreeView.setDisable(true);
            lastSequenceTreeView.setDisable(true);
            comboNameTF.setDisable(true);
            saveComboB.setDisable(true);
            comboNameTF.setText("");
            comboSeqDetails.setText("");
        } else {
            addRemoveComboBox.setDisableRemoveButton(false);
            firstSequenceTreeView.setDisable(false);
            lastSequenceTreeView.setDisable(false);
            comboNameTF.setDisable(false);
            saveComboB.setDisable(false);

            AcceleratorSeqCombo combo = (AcceleratorSeqCombo) item.getValue();
            List<String> sequences = combo.getConstituentNames();
            firstSequenceTreeView.selectElement(sequences.get(0));
            lastSequenceTreeView.selectElement(sequences.get(sequences.size() - 1));
            comboNameTF.setText(combo.getId());
            comboSeqDetails.setText("Length = " + String.format("%.3f", combo.getLength()) + " m.");
        }
    }

    @FXML
    private void updateComboSequences(ActionEvent event) {
        String newComboName = comboNameTF.getText();
        if ("".equals(comboNameTF.getText())) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Combo sequence name not provided");
            alert.setHeaderText("Combo sequence name not provided");
            alert.setContentText("Please enter a name for the Combo sequence.");
            alert.showAndWait();
            return;
        }

        AcceleratorSeq firstSequence = (AcceleratorSeq) firstSequenceTreeView.getSelectedNode();
        AcceleratorSeq lastSequence = (AcceleratorSeq) lastSequenceTreeView.getSelectedNode();

        if (lastSequence.getPosition() < firstSequence.getPosition() + firstSequence.getLength()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Combo sequence could not be created");
            alert.setHeaderText("Combo sequence could not be created");
            alert.setContentText("The last sequence should come after the first sequence.");
            alert.showAndWait();
            return;
        }

        Accelerator accelerator = getApplication().getDocument().getAccelerator();

        AcceleratorSeqCombo combo = AcceleratorSeqCombo.getInstanceForRange(newComboName, firstSequence, lastSequence);
        if (combo != null) {
            // When updating an existing combo sequence, first remove it.
            if (comboTreeView.getSelectedNode() != null) {
                accelerator.removeComboSequence(comboTreeView.getSelectedNodeId());
            }
            // Adding the new combo sequence.
            accelerator.addComboSequence(combo);
            comboTreeView.addElement(combo);
            // Make sure the treeview is updated before selectElement is called.
            Platform.runLater(() -> comboTreeView.selectElement(newComboName));
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Combo sequence could not be created");
            alert.setHeaderText("Combo sequence could not be created");
            alert.setContentText("Please check the selected sequences.");
            alert.showAndWait();
        }
    }

    private void addNewCombo() {
        comboTreeView.clearSelection();
        comboNameTF.setText("");
        firstSequenceTreeView.clearSelection();
        lastSequenceTreeView.clearSelection();
        firstSequenceTreeView.setDisable(false);
        lastSequenceTreeView.setDisable(false);
        comboNameTF.setDisable(false);
        saveComboB.setDisable(true);
    }

    private void removeCombo() {
        if (comboTreeView.getSelectedNode() != null) {
            Accelerator accelerator = getApplication().getDocument().getAccelerator();
            accelerator.removeComboSequence(comboTreeView.getSelectedNodeId());
            comboTreeView.clearSelection();
            comboNameTF.setText("");
            firstSequenceTreeView.clearSelection();
            lastSequenceTreeView.clearSelection();
            firstSequenceTreeView.setDisable(true);
            lastSequenceTreeView.setDisable(true);
            comboNameTF.setDisable(true);
            saveComboB.setDisable(true);
            comboTreeView.update(accelerator);
        }
    }

    private void calculateComboLenght() {
        AcceleratorSeq firstSequence = (AcceleratorSeq) firstSequenceTreeView.getSelectedNode();
        AcceleratorSeq lastSequence = (AcceleratorSeq) lastSequenceTreeView.getSelectedNode();

        if (firstSequence == null || lastSequence == null) {
            return;
        }

        if (lastSequence.getPosition() < firstSequence.getPosition() + firstSequence.getLength()) {
            comboSeqDetails.setText(String.format("The last sequence should come after the first sequence. First sequence finishes at %.3f m and last sequence starts at %.3f m.",
                    firstSequence.getPosition() + firstSequence.getLength(), lastSequence.getPosition()));
            saveComboB.setDisable(true);
            return;
        }

        AcceleratorSeqCombo combo = AcceleratorSeqCombo.getInstanceForRange(comboNameTF.getText(), firstSequence, lastSequence);
        if (combo != null) {
            comboSeqDetails.setText("Length = " + String.format("%.3f", combo.getLength()) + " m.");
            saveComboB.setDisable(false);
        } else {
            comboSeqDetails.setText("A combo sequence cannot be created with the current selection.");
            saveComboB.setDisable(true);
        }
    }

    /*
     ***************************************
     * Power Supplies Tab related methods. *
     ***************************************
     */
    private void updatePowerSupplies(Accelerator accelerator) {
        powerSuppliesList.clear();
        accelerator.getMagnetMainSupplies().stream().forEach((powerSupply) -> powerSuppliesList.add(powerSupply.getId()));
    }

    private void addNewPowerSupply() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Add New Power Supply");
        dialog.setHeaderText("Adding New Power Supply");
        dialog.setContentText("Please enter your ID of the power supply:");

        Optional<String> psId = dialog.showAndWait();
        if (psId.isPresent()) {
            Accelerator accelerator = getApplication().getDocument().getAccelerator();
            MagnetMainSupply ps = new MagnetMainSupply(accelerator);
            ps.setId(psId.get());
            // Add basic channels - with empty signals
            Collection<String> defaultHandles = ps.getDefaultHandles();
            for (String handle : defaultHandles) {
                ps.getChannelSuite().putChannel(handle, "", true);
            }
            accelerator.putMagnetMainSupply(ps);
            powerSuppliesTreeView.update(accelerator);

            // Make sure the treeview is updated before selectElement is called.
            Platform.runLater(() -> powerSuppliesTreeView.selectElement(psId.get()));

            updatePowerSupplies(accelerator);
        }
    }

    private void removePowerSupply() {
        TreeItem<MagnetPowerSupply> item = powerSuppliesTreeView.getSelectedItem();
        if (item != null && item.getParent() != null) {
            item.getParent().getChildren().remove(item);
            powerSuppliesTreeView.refresh();
            MagnetPowerSupply node = item.getValue();
            if (node != null) {
                Accelerator accelerator = getApplication().getDocument().getAccelerator();
                try {
                    Field m_mmsField = Accelerator.class.getDeclaredField("magnetMainSupplies");
                    m_mmsField.setAccessible(true);
                    Map<String, MagnetMainSupply> magnetMainSupplies = (Map<String, MagnetMainSupply>) m_mmsField.get(accelerator);
                    magnetMainSupplies.remove(node.getId());
                } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(PropertiesTableWrapper.class.getName()).log(Level.SEVERE, null, ex);
                }
                itemSelectionChangedPS(powerSuppliesTreeView.getSelectedItem());

                updatePowerSupplies(accelerator);
            }
        }
    }

    /*
     ***************************************
     * Device Manager Tab related methods. *
     ***************************************
     */
    private void updateDM(Accelerator accelerator) {
        deviceManagerTableWrapper = new DeviceManagerTableWrapper(accelerator);
        deviceManagerTableWrapper.setTableView(deviceManagerTV, iconColumnDM, typeColumDM, softTypeColumDM, classColumnDM);
    }

    @FXML
    private void addDeviceType(ActionEvent event) {
        deviceManagerTableWrapper.addDeviceType();
        deviceManagerTV.refresh();
    }

    @FXML
    private void removeDeviceType(ActionEvent event) {
        DeviceTypeWrapper selectedItem = deviceManagerTV.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            deviceManagerTableWrapper.removeDeviceType(selectedItem.typeProperty().getValue(), selectedItem.softTypeProperty().getValue());
            deviceManagerTV.refresh();
        }
    }

    @FXML
    private void reloadDeviceTypes(ActionEvent event) {
        updateDM(getApplication().getDocument().getAccelerator());
    }

    /*
     **************************************
     * Model Manager Tab related methods. *
     **************************************
     */
    private void updateMM(Accelerator accelerator) {
        elementMappingTableWrapper = new ElementMappingTableWrapper(accelerator);
        elementMappingTableWrapper.setDefaultElementsTableView(defaultTypesTV, iconColumnDefMM, typeColumDefMM, classColumnDefMM);
        elementMappingTableWrapper.setTableView(modelManagerTV, iconColumnMM, typeColumMM, classColumnMM);
    }

    @FXML
    private void addElementType(ActionEvent event) {
        elementMappingTableWrapper.addElementType();
        modelManagerTV.refresh();
    }

    @FXML
    private void removeElementType(ActionEvent event) {
        ElementModelWrapper selectedItem = modelManagerTV.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            elementMappingTableWrapper.removeElementType(selectedItem.typeProperty().getValue());
            modelManagerTV.refresh();
        }
    }

    @FXML
    private void reloadElementTypes(ActionEvent event) {
        updateMM(getApplication().getDocument().getAccelerator());
    }


    /*
     **********************************************
     * Simulation parameters Tab related methods. *
     **********************************************
     */
//    @FXML
//    private void addSmParams(ActionEvent event) {
//        // TODO
//    }
//
//    @FXML
//    private void removeSimParams(ActionEvent event) {
//        // TODO
//    }
}
